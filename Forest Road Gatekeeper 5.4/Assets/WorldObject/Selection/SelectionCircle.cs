﻿using UnityEngine;
using System.Collections;

public class SelectionCircle : MonoBehaviour {

	void Start () {
		WorldObject parent = this.GetComponentInParent<WorldObject>();
		if(parent == null) {
			Destroy(this);
			return;
		}
		parent.setSelectionObj(this);
	}
}
