﻿using UnityEngine;
using System.Collections;
using RTS;

public class TempUnit : Unit {

	protected override void Start() {
		base.Start();
	}

	protected override void Update() {
		base.Update();
	}

	public override bool CanAttack() {
		return true;
	}

	protected override void UseWeapon() {
		base.UseWeapon();
		Vector3 spawnPoint = transform.position;
		spawnPoint.x += (2.1f * transform.forward.x);
		spawnPoint.y += 1.4f;
		spawnPoint.z += (2.1f * transform.forward.z);
		GameObject gameObject = (GameObject) Instantiate(ResourceManager.GetMiscObject("TankShell"), spawnPoint, transform.rotation);
		Projectile projectile = gameObject.GetComponentInChildren<Projectile>();
		projectile.SetDamage(DamageType.Hack, dmgHack);
		projectile.SetRange(0.99f * weaponRange);
		projectile.SetTarget(target);
	}

}
