﻿using UnityEngine;
using System.Collections;
using RTS;

public class FlyingUnit : Unit {

	protected override void Start() {
		base.Start();
		isFlying = true;
	}

	public override bool CanAttack() {
		return false;
	}

}
