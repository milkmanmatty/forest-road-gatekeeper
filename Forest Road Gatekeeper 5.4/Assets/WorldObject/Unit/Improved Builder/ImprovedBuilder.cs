using UnityEngine;
 
public class ImprovedBuilder : Unit {
 
    public int buildSpeed;
 
    private Building currentProject;
	private float projectSizeX = 0;

    private float amountBuilt = 0.0f;
 
    /*** Game Engine methods, all can be overridden by subclass ***/
 
    protected override void Start () {
        base.Start();
        actions = new string[] {"Keep", "ShrineOfHealing", "Archer Tower" };
    }
 
    protected override void Update () {
        base.Update();

		
		if(this.state == RTS.UnitState.MovingToBuild) {
			if(currentProject == null) {
				target = null;
				this.state = RTS.UnitState.Idle;
				return;
			}
			if(Vector3.Distance(this.transform.position, currentProject.transform.position) < (projectSizeX+this.buildDist)) {
				this.state = RTS.UnitState.Building;
			}
			if(WithinAcceptableDistanceToWaypoint() && waypoint < path.Length - 1) {
				waypoint++;
				return;
			}

		} else if(this.state == RTS.UnitState.Building && currentProject && currentProject.UnderConstruction()) {
			amountBuilt += buildSpeed * Time.deltaTime;
			int amount = Mathf.FloorToInt(amountBuilt);
			if(amount > 0) {
				amountBuilt -= amount;
				currentProject.Construct(amount);
				if(!currentProject.UnderConstruction()){

					if(targetBaseLocID != -1) {
						currentProject.GetComponent<Building>().BaseLocation_ReleaseLoc(targetBaseLocID);
						this.targetBaseLocID = -1;
					}

					this.state = RTS.UnitState.Idle;
					currentProject = null;
					projectSizeX = 0;
                }
			}
		}
    }
 
    /*** Public Methods ***/
 
    public override void SetBuilding (Building project) {
        base.SetBuilding (project);
        currentProject = project;
		projectSizeX = currentProject.buildPlacer.GetComponent<Renderer>().bounds.extents.x;
		project.BaseContactLocations_Update();

        this.state = RTS.UnitState.MovingToBuild;
		target = project;
		SetDestination(target.transform.position);
		TryUpdateTarget(project);
    }
 
    public override void PerformAction (string actionToPerform) {
        base.PerformAction (actionToPerform);
		if(player.IsFindingBuildingLocation()){
			player.CancelBuildingPlacement();
		}
        CreateBuilding(actionToPerform);
    }
 
    protected override void MoveToWaypoint() {
        base.MoveToWaypoint();
        amountBuilt = 0.0f;
    }
	
	public override void MouseClick (GameObject hitObject, Vector3 hitPoint, Player controller) {
		//only handle input if owned by a human player and currently selected
		if(player.human && isSelected && hitObject.name != "Ground" && hitObject.name != "Fog") {
			Building building = hitObject.transform.parent.GetComponent<Building>();
			if(building) {
				if(building.UnderConstruction()) {
					SetBuilding(building);
					return;
				}
			}
		}
		base.MouseClick(hitObject, hitPoint, controller);
	}
 
    private void CreateBuilding(string buildingName) {
		Vector3 buildPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z + 10);
		if(player){
			if(!player.CreateBuilding(buildingName, buildPoint, this, playingArea)){
				//Not enough resources
			}
		}
	}
}