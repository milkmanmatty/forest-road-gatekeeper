using UnityEngine;
using System.Collections;
using RTS;

public class Unit : WorldObject {

	public static int mobilePriority = 50;
	public static int stationaryPriority = 40;

	public float moveSpeed = 3f;
	private float rotateSpeed = 100;
	public int populationCost = 1;

	public float buildDist = 5f;
	public bool isHero = false;


	//MOVEMENT VARIABLES
	public Vector3 destination;
	public Vector3 Destination { get { return destination; } }

	//Pathfinding
	protected NavMeshAgent agent;
	protected NavMeshObstacle obstacle;
	public int priority = 50;
	protected Vector3 lastPosition;
	public float destinationAllowance = 2.0f;
	public float targetSize = 1f;
	public int targetBaseLocID = -1;

	public int waypoint = 0;
	public Vector3[] path;
	public bool waitingForNewPath = false;

	public bool AcceptableRange = false;
	//public bool tracked = false;

	//delayed updates.
	protected float timeBetweenDelayedUpdates = 0.25f;

	/****************
	* Unity Methods *
	****************/

	protected override void Awake() {
		base.Awake();
		destination = ResourceManager.InvalidPosition;
	}

	protected override void Start() {
		base.Start();
		this.isUnit = true;
		if(!isFlying) {
			agent = GetComponent<NavMeshAgent>();
		}
		obstacle = GetComponent<NavMeshObstacle>();

		if(player) {
			player.AddToPop(populationCost);
			StartDelayedUpdate();
		}
	}


	protected override void Update() {
		base.Update();

		if(isFlying) {
			if(target != null && (state == UnitState.Moving || state == UnitState.MovingToAttack)) {
				MoveToTarget();
			}
		} else {
			if(waitingForNewPath && !agent.pathPending) {
				waitingForNewPath = false;
				path = agent.path.corners;
				if(path.Length == 1) {
					target = Hideout.GetHideoutObject();
					if(!isFlying) {
						setAgentPriority(true);
					}
					TryUpdateLocation(target.transform.position, false);
				}
			}
			if(path != null && path.Length > 0 && (state == UnitState.Moving || state == UnitState.MovingToAttack || state == UnitState.MovingToBuild)) {
				MoveToWaypoint();
			}
		}
	}

	IEnumerator DelayedUpdate() {
		while(true) {
			DecisionMaking();
			yield return new WaitForSeconds(timeBetweenDelayedUpdates);
		}
	}


	private void DecisionMaking() {
		this.CalculateBounds();

        // This unit has a target
        if(target != null) {
			switch(state) {
				case UnitState.Attacking:
					if(TargetInWeaponRange()) {
						if(ReadyToFire()) {
							UseWeapon();
						}
					} else {
						if(!isFlying) {
							setAgentPriority(true);
						}
						if(TryUpdateTarget(target)) {
							state = UnitState.MovingToAttack;
						}
					}
					break;

				case UnitState.Moving:

					if(path.Length == 1) {
						target = Hideout.GetHideoutObject();
						if(!isFlying) {
							setAgentPriority(true);
						}
						TryUpdateLocation(target.transform.position, false);
						break;
					}

					if(this.CanAttack() && this.timeSinceLastScan > this.timeBetweenScans && FindNearbyEnemies()) {
						if(nearbyObjects[0] != Hideout.GetHideoutObject()) {
							if(TryUpdateTarget(nearbyObjects[0])) {
								state = UnitState.MovingToAttack;
							}
							break;
						}
					}

					if(isFlying) {
						//Unit should die before this happens
						if(WithinAcceptableDistanceToTarget()) {
							state = UnitState.Idle;
						}
						break;
					}

					//Move on to the next waypoint
					if(WithinAcceptableDistanceToWaypoint() && waypoint < path.Length-1) {
						waypoint++;
						return;
					}

					if(WithinAcceptableDistanceToWaypoint() && waypoint == path.Length - 1) {
						//Update target position
						TryUpdateTarget(target);
					}

					break;

				case UnitState.MovingToAttack:
					if(target == Hideout.GetHideoutObject()) {
						state = UnitState.Moving;
					}

					if(TargetInWeaponRange()) {
						if(isMelee) {
							if(ReadyToFire()) {
								UseWeapon();
							}

							//Help pathfinding out, do not stop moving until we are at appropriate spot.
							if(WithinAcceptableDistanceToTarget()) {
								state = UnitState.Attacking;
								SnapRotationToTarget();
								return;
							}
							break;
						}

						//ranged
						if(ReadyToFire()) {
							UseWeapon();
						}
						state = UnitState.Attacking;
						SnapRotationToTarget();
						break;
					}

					//Move on to the next waypoint
					if(WithinAcceptableDistanceToWaypoint() && waypoint < path.Length - 1) {
						waypoint++;
						return;
					}

					if(WithinAcceptableDistanceToWaypoint() && waypoint == path.Length - 1) {
						//Update target position
						TryUpdateTarget(target);
					}

					break;
			}

		//This unit has no target
		} else {
			if(player.human) {
				if(Destination != ResourceManager.InvalidPosition) {
					if(state == UnitState.Moving) {
						if(WithinAcceptableDistanceToWaypoint() && waypoint < path.Length - 1) {
							waypoint++;
							return;
						} else if(WithinAcceptableDistanceToWaypoint() && waypoint == path.Length-1) {
							state = UnitState.Idle;
						}
					}
				} else if(state == UnitState.Idle) {
					if(this.CanAttack() && this.timeSinceLastScan > this.timeBetweenScans && FindNearbyEnemies()) {
						if(TryUpdateTarget(nearbyObjects[0])) {
							state = UnitState.MovingToAttack;
						}
					}
				}

				//Let's not override the state if building or moving to build etc.
				else if(state == UnitState.Attacking || state == UnitState.Moving || state == UnitState.MovingToAttack) {
					state = UnitState.Idle;
				}
			} else {
				state = UnitState.Moving;
				target = Hideout.GetHideoutObject();
				if(!isFlying) {
					setAgentPriority(true);
				}
				TryUpdateLocation(target.transform.position, false);
			}
		}
	}

	protected override void OnGUI() {
		base.OnGUI();
	}

	void OnDisable() {
		if(targetBaseLocID != -1) {
			if(target != null) {
				target.GetComponent<Building>().BaseLocation_ReleaseLoc(targetBaseLocID);
			} else {
				Debug.Log(objectName+ " target is null. targetBaseLocID "+ targetBaseLocID);
			}
        }
		if(player) {
			player.RemoveFromPop(populationCost);
		}
		
	}

	/*************
	* COLLISIONS *
	*************/

	//We are only looking for WorldObject collisions here. WorldObjects are Kinematic therefore we can't use OnCollisionEnter
	//For kinematic on kinematic action we have to use OnTriggerEnter()
	public override void OnTriggerEnter(Collider col) {
		base.OnTriggerEnter(col);

		WorldObject temp = col.transform.GetComponent<WorldObject>();
		if(temp == null) {
			return;
		}
		
		if(temp.isUnit) {
			Unit unit = col.transform.GetComponent<Unit>();

			if(unit.priority > this.priority) {
				//This unit is stationary, so it aint moving.
				return;
			}

			if(unit.priority == this.priority && this.priority == stationaryPriority) {
				//Both units are stationary so neither are moving (yet).
				return;
			}

			if(unit.priority < this.priority) {
				//This unit has to move, so recalculate its path

				if(state == UnitState.MovingToAttack || state == UnitState.MovingToBuild || state == UnitState.Attacking) {
					if(!waitingForNewPath) {
						TryUpdateTarget(target);
					} else {
						Debug.Log(this.transform.name + ": Is waiting for a path to " + destination);
					}
				} else if(state == UnitState.Moving) {
					if(!waitingForNewPath) {
						
						TryUpdateLocation(destination);
					} else {
						Debug.Log(this.transform.name + ": Is waiting for a path to " + destination);
					}
                } else if(state == UnitState.Idle) {
					//Debug.Log(this.transform.name + ": Rerouting self");
					RerouteSelf(col.bounds.center);
				}
				return;
			}

			if(unit.priority == this.priority && this.priority == mobilePriority) {
				//Both units are mobile, move the one with the higher objID.
				if(this.ObjID > unit.ObjID) {
					//Debug.Log(objectName+" rerouting self");
					RerouteSelf(col.bounds.center);
				}
				return;
			}

		}
	}

	private void RerouteSelf(Vector3 contact) {
		Vector3 dir = contact - transform.position;
		dir = -dir.normalized;

		Vector3 reroute = transform.position + (dir * (3*destinationAllowance));

		//Debug.Log(transform.name + " rerouting to " + reroute);

		if(state == UnitState.MovingToAttack || state == UnitState.MovingToBuild || state == UnitState.Attacking) {
			TryUpdateLocation(reroute, false);
		}

		if(TryUpdateLocation(reroute)) {
			state = UnitState.Moving;
		}
	}

	/***************
	** UNIT STATS **
	***************/

	protected override void AddStatModifiers() {

		base.AddStatModifiers();

		int speedMod = GameControl.control.speed_Modifier;
		this.moveSpeed += this.moveSpeed / 100.0f * speedMod;

	}


	/********
	** HUD **
	********/

	public override void SetHoverState(GameObject hoverObject) {
		base.SetHoverState(hoverObject);
		//only handle input if owned by a human player and currently selected
		if(player && player.human && isSelected) {
			if(hoverObject.name == "Ground") {
				player.hud.SetCursorState(CursorState.Move);
			}
		}
	}

	/***************
	** USER INPUT **
	***************/

	public override void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller) {
		base.MouseClick(hitObject, hitPoint, controller);

		//only handle input if owned by a human player and currently selected
		if(player && player.human && isSelected) {
			if((hitObject.name == "Ground" || hitObject.name == "Fog") && hitPoint != ResourceManager.InvalidPosition) {
				TryUpdateLocation(new Vector3(hitPoint.x, 0, hitPoint.z));
				this.state = UnitState.Moving;
			}
		}
	}


	public void SetDestinationClearTarget(Vector3 newDestination) {
		SetDestination(newDestination);
		target = null;
	}


	/**************
	* PATHFINDING *
	**************/

	protected bool TryUpdateTarget(WorldObject wo) {
		if(!agent.pathPending) {
			UpdateTarget(wo);
			if(!isFlying) {
				agent.SetDestination(destination);
				waitingForNewPath = true;
				waypoint = 0;
			}
			return true;
		}
		return false;
	}

	protected bool TryUpdateLocation(Vector3 loc, bool clearTarget = true) {

		if(isFlying) {
			this.targetSize = 1.0f;
			this.targetBaseLocID = -1;
			if(clearTarget) {
				SetDestinationClearTarget(loc);
			} else {
				SetDestination(loc);
			}
			return true;

		} else if(!agent.pathPending) {

			if(this.player.human) {
				if(target && target.isBuilding && targetBaseLocID != -1) {
					target.GetComponent<Building>().BaseLocation_ReleaseLoc(targetBaseLocID);
					targetBaseLocID = -1;
					this.targetSize = 1.0f;
				}
			} else {
				//Reset target vars
				this.targetSize = 1.0f;
				this.targetBaseLocID = -1;
			}

			if(clearTarget) {
				SetDestinationClearTarget(loc);
			} else {
				SetDestination(loc);
			}

			agent.SetDestination(loc);
			waitingForNewPath = true;
			waypoint = 0;

			return true;
		}
		return false;
	}


	/*************
	** MOVEMENT **
	*************/

	public virtual void SetDestination(Vector3 newDest) {
		newDest.y = this.transform.position.y;
		destination = newDest;
	}

	public bool WithinAcceptableDistanceToWaypoint() {
		if(path.Length > 0) {
			Vector3 direction = path[waypoint] - transform.position;
			if(direction.sqrMagnitude < destinationAllowance * destinationAllowance) {
				return true;
			}
		}
		return false;
	}


	public bool WithinAcceptableDistanceToTarget() {

		Vector3 direction = target.transform.position - transform.position;
		if(direction.sqrMagnitude < (targetSize + destinationAllowance) * (targetSize + destinationAllowance)) {

			//Make agent an obstacle
			if(!isFlying) {
				setAgentPriority(false);
				AcceptableRange = true;
			}
			return true;
		}

		//Make agent moveable
		if(!isFlying) {
			setAgentPriority(true);
			AcceptableRange = false;
		}
		return false;
	}


	public override bool UpdateTarget(WorldObject newTarget) {

		if(target.isBuilding && targetBaseLocID != -1) {
			target.GetComponent<Building>().BaseLocation_ReleaseLoc(targetBaseLocID);
			targetBaseLocID = -1;
        }

		this.target = newTarget;

		if(isFlying) {
			if(newTarget.isBuilding) {
				Building buildingTarget = newTarget.GetComponent<Building>();
				this.targetSize = buildingTarget.buildingPlacementBounds.extents.x + buildingTarget.buildingPlacementBounds.extents.z;
			} else {
				this.targetSize = 2.0f;
			}
			this.targetBaseLocID = -1;
			this.destination = newTarget.transform.position;
			return true;
		}

		if(isMelee && newTarget.isBuilding) {
			Building buildingTarget = newTarget.GetComponent<Building>();
			if(buildingTarget.BaseLocation_HasFreeLoc()) {
				this.targetSize = buildingTarget.buildingPlacementBounds.extents.x + buildingTarget.buildingPlacementBounds.extents.z;
				this.targetBaseLocID = buildingTarget.BaseLocation_ObtainClosestFreeLocID(this.transform.position);
				this.destination = buildingTarget.baseContactLocations[targetBaseLocID];
                if(destination != ResourceManager.InvalidPosition) {
					return true;
				}
			}
		} else if(newTarget.isUnit) {
			this.targetSize = 2.0f;
			this.targetBaseLocID = -1;
			this.destination = newTarget.transform.position;
        }
		return false;
	}


	/// <summary>
	/// Rotates this Unit towards its target and returns whether further rotation is required.
	/// </summary>
	/// <returns>true if this unit is facing its target, false otherwise.</returns>
	private bool TurnToTarget() {


		Vector3 aim = (Destination - transform.position).normalized;
		Quaternion targetRotation = Quaternion.LookRotation(aim);

		transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed);

		Quaternion inverseTargetRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(Destination - transform.position), rotateSpeed);

		if(transform.rotation == targetRotation || transform.rotation == inverseTargetRotation) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Snaps the rotation of this unit to the target.
	/// </summary>
	private void SnapRotationToTarget() {
		transform.LookAt(target.transform);
	}

	/// <summary>
	/// Moves this unit to the current waypoint.
	/// </summary>
	protected virtual void MoveToWaypoint() {

		if(waypoint > path.Length - 1) {
			return;
		}

		this.transform.position = Vector3.MoveTowards(transform.position, path[waypoint], Time.deltaTime * this.moveSpeed);

		TurnToTarget();

		//Keep Health bar on unit
		CalculateBounds();
	}

	/// <summary>
	/// Moves this unit to the current target. Does not use pathfinding!
	/// </summary>
	protected virtual void MoveToTarget() {

		if(WithinAcceptableDistanceToTarget()) {
			return;
		}

		this.transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * this.moveSpeed);

		TurnToTarget();

		//Keep Health bar on unit
		CalculateBounds();
	}

	/**********
	** OTHER **
	**********/

	public virtual void SetBuilding(Building creator) {
		//specific initialization for a unit can be specified here
	}

	protected void setAgentPriority(bool low) {
		if(low) {
			priority = mobilePriority;
            agent.avoidancePriority = mobilePriority;
			obstacle.enabled = false;
			agent.enabled = true;
		} else {
			priority = stationaryPriority;
            agent.avoidancePriority = stationaryPriority;
			agent.enabled = false;
			obstacle.enabled = true;
		}
	}

	public void StartDelayedUpdate() {
		StartCoroutine(DelayedUpdate());
	}

}
