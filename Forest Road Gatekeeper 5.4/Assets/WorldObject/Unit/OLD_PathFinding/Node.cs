﻿using UnityEngine;
using System.Collections;

public class Node : IHeapItem<Node> {

	public bool traversable;
	public Vector3 worldPosition;
	public int gridX;
	public int gridY;

	public int movementPenalty;
	private int heapIndex;

	public int gCost;
	public int hCost;
	public int fCost {
		get {
			return gCost + hCost;
		}
	}

	public Node parent;

	public Node(bool isTraversable, Vector3 worldPos, int x, int y, int penalty) {
		traversable = isTraversable;
		worldPosition = worldPos;
		gridX = x;
		gridY = y;
		movementPenalty = penalty;
	}

	public int HeapIndex {
		get {
			return heapIndex;
		}
		set {
			heapIndex = value;
		}
	}

	public int CompareTo(Node nodeToCompare) {
		int compare = fCost.CompareTo(nodeToCompare.fCost);
		if(compare == 0) {
			compare = hCost.CompareTo(nodeToCompare.hCost);
		}
		return -compare;
	}
}
