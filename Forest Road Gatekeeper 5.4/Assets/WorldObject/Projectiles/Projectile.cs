using UnityEngine;
using System.Collections;
using RTS;
 
public class Projectile : MonoBehaviour {
 
    public float velocity = 25;

	protected DamageType dmgType = DamageType.Hack;
	protected int HackDmg = 0;
	protected int PieceDmg = 0;
	protected int MagicDmg = 0;

	private float range = 1;
    private WorldObject target;
	private Player sender;

	private bool runOnce = true;

	void Start() {
		if(target == null) {
			Debug.Log(gameObject.name + " has destroyed itself [No target]");
			Destroy(gameObject);
			return;
		}
		Vector3 targetPos = target.transform.position;
		targetPos.y += 1.5f;
		this.transform.LookAt(targetPos);
	}
 
    void Update () {
        if(range>0) {
            float positionChange = Time.deltaTime * velocity;
            range -= positionChange;
            transform.position += (positionChange * transform.forward);
        } else {
			Debug.Log(gameObject.name + " has destroyed itself [Exceeds Range]");
            Destroy(gameObject);
        }
    }
 
    public void SetRange(float range) {
        this.range = range * 1.5f;
    }
	
	public void SetDamage(DamageType type, int dmg) {
		switch(type) {
			case DamageType.Hack:
				HackDmg = dmg;
				break;
			case DamageType.Piece:
				PieceDmg = dmg;
				break;
			case DamageType.Magic:
				MagicDmg = dmg;
				break;
		}
    }
	
	public void SetVelocity(float velo) {
        velocity = velo;
    }
 
    public void SetTarget(WorldObject target) {
        this.target = target;
    }

	public void SetPlayer(Player owner) {
		this.sender = owner;
	}

	
	public void OnTriggerEnter(Collider other) {
		if(other.gameObject.layer == 12 || other.gameObject.layer == 10) {			
			WorldObject hit = other.transform.GetComponentInParent<WorldObject>();
			if(hit && !hit.isBuilding && !hit.isUnit) {
				// we hit a tree or rock.
				return;
			}
			if(hit && hit.isBuilding && hit.GetComponent<Building>().IsTempBuilding) {
				// we hit a temp building.
				return;
			}
			if(hit && !hit.IsOwnedBy(sender) && runOnce) {
				runOnce = false;
				InflictDamage(hit);
				Destroy(gameObject);
			}
		}
	}

	/*
	private bool HitSomething() {
		if(target && target.GetSelectionBounds().Contains(transform.position)) {
			return true;
		}
        return false;
    }
	*/
 
    private void InflictDamage(WorldObject victim) {
		victim.TakeDamage(HackDmg, DamageType.Hack);
		victim.TakeDamage(PieceDmg, DamageType.Piece);
		victim.TakeDamage(MagicDmg, DamageType.Magic);
	}
}