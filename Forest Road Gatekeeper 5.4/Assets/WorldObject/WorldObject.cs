using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RTS;

public class WorldObject : MonoBehaviour {

	//DEBUGGING VARS
	public WorldObject target = null;
	public UnitState state = UnitState.Idle;

	//Variables
	public string objectName = "WorldObject";
	public Texture2D buildImage;
	
	public int goldCost = 100;
	public int dpCost = 0;
	public int sellValue = 50;
	
	public float hitPoints = 100.0f;
	public float maxHitPoints = 100.0f;
	public float weaponRange = 10.0f;
	protected float weaponRangeModifier = 1.0f;
	public float EffectiveRange {
		get { return (weaponRange * weaponRangeModifier); } }

	public int lineOfSight = 8;
	private Fog hit;
	public bool visible = true;

	public int dmgHack = 0;
	public int dmgPiece = 0;
	public int dmgMagic = 0;
		
	public float hackArmour = 0.0f;
	public float pieceArmour = 0.0f;
	public float magicArmour = 0.0f;
	
	public bool isBuilding = false;
	public bool isUnit = false;
	public bool isFlying = false;
	public bool isMelee = false;
	
	public float weaponRechargeTime = 1.0f;
	private float currentWeaponChargeTime;	

	protected GUIStyle healthStyle = new GUIStyle();
	protected float healthPercentage = 1.0f;

	private SelectionCircle selectionObj;
	protected Bounds selectionBounds;
	protected Rect playingArea = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
	
	protected Player player;
	protected string[] actions = {};
	public bool isSelected = false;
	
	public virtual bool IsActive { get { return true; } }
		
	private List<Material> oldMaterials = new List<Material>();

	//we want to restrict how many nearby unit scans are made to help with game performance
	//the default time at the moment is a tenth of a second
	protected float timeSinceLastScan = 0.0f;
	protected float timeBetweenScans = 0.3f;

	protected List<WorldObject> nearbyObjects;

	public static int NextObjectID = 0;
	public int ObjID;

	protected virtual void Awake() {
		selectionBounds = ResourceManager.InvalidBounds;
		CalculateBounds();
		if(NextObjectID == 0) {
			NextObjectID++;
		}
		ObjID = NextObjectID;
		NextObjectID++;		
	}
	 
	protected virtual void Start () {
		SetPlayer();
		if(player){
			SetTeamColor();
		}

		/* Add current wave HP and Attack modifiers to instance */
		if(GameControl.control.currentWave > 0) {
			AddStatModifiers();
		}		

	}
	 
	protected virtual void Update () {
		if(CanAttack()) {
			currentWeaponChargeTime += Time.deltaTime;
			timeSinceLastScan += Time.deltaTime;
		}
	}
	 
	protected virtual void OnGUI() {
		if(isSelected){
			DrawSelection();
		}
	}


	/*************
	* UNIT STATS *
	*************/

	protected virtual void AddStatModifiers() {

		int statPercent = 0;

		if(this.player == GameControl.control.Gamma) {
			statPercent = GameControl.control.gamma_DmgHPModifier;
		} else if(this.player == GameControl.control.Beta) {
			statPercent = GameControl.control.beta_DmgHPModifier;
        } else if(this.player == GameControl.control.Gamma) {
			statPercent = GameControl.control.alpha_DmgHPModifier;
        }

		int hp = (int) (this.maxHitPoints / 100.0f * statPercent);
		AddHealth(hp);

		int hack = (int) (this.dmgHack / 100.0f * statPercent);
		int piece = (int) (this.dmgPiece / 100.0f * statPercent);
		int magic = (int) (this.dmgMagic / 100.0f * statPercent);
		AddAttack(hack, piece, magic);

		float armAdd = GameControl.control.armour_Modifier;
		this.hackArmour += armAdd;
		this.pieceArmour += armAdd;
		this.magicArmour += (armAdd/2.0f);

	}

	protected void AddHealth(int hp) {
		this.maxHitPoints += hp;
		this.hitPoints += hp;
	}

	protected void AddWeaponRange(float range) {
		this.weaponRange += range;
	}

	protected void AddLoS(int range) {
		this.lineOfSight += range;
		Revealer rev = GetComponentInChildren<Revealer>();
		if(rev) {
			rev.UpdateRange();
		}
	}

	protected void AddAttack(int hack, int piece, int magic) {
		this.dmgHack += hack;
		this.dmgPiece += piece;
		this.dmgMagic += magic;
	}


	/************
    * SELECTION *
    ************/

	/// <summary>
	/// Use this method to determine selection or deselection.
	/// This method checks to ensure that it is not contained in the owning player's SelectedObjects list before adding itself to the list.
	/// Similiarly it also checks that it is contained in the owning player's SelectedObjects list before removing itself from the list.
	/// </summary>
	/// <param name="selected"></param>
	/// <param name="playingArea"></param>
	public virtual void SetSelection(bool selected, Player p, Rect playingArea) {
		if(selected){
            if(!p.isObjectSelected(this)) {
				this.playingArea = playingArea;
				isSelected = selected;
				p.addWorldObjectToSelected(this);
            }
		} else if(p.isObjectSelected(this)) {
			isSelected = selected;
			p.removeWorldObjectSelection(this);
        }
		this.selectionObj.GetComponent<Renderer>().enabled = selected;
	}

	/// <summary>
	/// Sets the isSelected property to false. 
	/// </summary>
	public virtual void deselect() {
		isSelected = false;
		if(selectionObj) {
			this.selectionObj.GetComponent<Renderer>().enabled = false;
		}
	}

	public void setSelectionObj(SelectionCircle obj) {
		this.selectionObj = obj;
	}

	private void ChangeSelection(WorldObject worldObject) {
		player.removeWorldObjectSelection(this);
		if(!player.isObjectSelected(worldObject)) {
			player.addWorldObjectToSelected(worldObject);
		}
	}

	private void DrawSelection() {
		GUI.skin = ResourceManager.SelectBoxSkin;
		Rect selectBox = WorkManager.CalculateHealthBarPos(selectionBounds);

		selectBox.y = (playingArea.height - selectBox.y + ResourceManager.ORDERS_BAR_HEIGHT);

		//Draw the health bar
		GUI.BeginGroup(playingArea);
		DrawHealthBar(selectBox, "");
		GUI.EndGroup();
	}

	public void CalculateBounds() {
		selectionBounds = new Bounds(transform.position, Vector3.zero);
		foreach(Renderer r in GetComponentsInChildren<Renderer>()) {
			selectionBounds.Encapsulate(r.bounds);
		}
	}

	public Bounds GetSelectionBounds() {
		return selectionBounds;
	}


	/**********
    * Actions *
    **********/

	public string[] GetActions() {
		return actions;
	}
	 
	public virtual void PerformAction(string actionToPerform) {
		//it is up to children with specific actions to determine what to do with each of those actions
	}
	
	/// <summary>
	/// Right Click
	/// </summary>
	/// <param name="hitObject"></param>
	/// <param name="hitPoint"></param>
	/// <param name="controller"></param>
	public virtual void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller) {
		//only handle input if currently selected
		if(isSelected && hitObject.name != "Ground" && hitObject.name != "Fog") {
			WorldObject worldObject = hitObject.transform.GetComponentInParent<WorldObject>();
			
			//clicked on another selectable object
			if(worldObject) {
				Resource resource = hitObject.transform.parent.GetComponent<Resource>();
				if(resource && resource.isEmpty()){
					return;
				}
				Player owner = hitObject.transform.root.GetComponent<Player>();
				
				//the object is controlled by a player
				if(owner != null) {
				
					//start attack if object is not owned by the same player and this object can attack, else select
					if((player.username != owner.username && owner.username != "Mother Nature") && CanAttack()){
						UpdateTarget(worldObject);
						if(isUnit) {
							if(TargetInWeaponRange()) {
								this.state = UnitState.Attacking;
							} else {
								this.state = UnitState.MovingToAttack;
							}
						} else if(isBuilding) {
							this.state = UnitState.Attacking;
                        }
					}	
				}
			}
		}
	}
	


	/*************
	* Health Bar *
	*************/
	
	protected virtual void CalculateCurrentHealth(float lowSplit, float highSplit) {
		healthPercentage = hitPoints / maxHitPoints;
		if(healthPercentage > highSplit){
			healthStyle.normal.background = ResourceManager.HealthyTexture;
		} else if(healthPercentage > lowSplit){
			healthStyle.normal.background = ResourceManager.DamagedTexture;
		} else {
			healthStyle.normal.background = ResourceManager.CriticalTexture;
		}
	}
	
	protected void DrawHealthBar(Rect selectBox, string label) {
		CalculateCurrentHealth(0.35f, 0.85f);
		healthStyle.padding.top = -20;
		healthStyle.fontStyle = FontStyle.Bold;
        GUI.Label(new Rect(selectBox.x, selectBox.y - 7, selectBox.width * healthPercentage, 5), label, healthStyle);
	}
	
	public virtual void SetHoverState(GameObject hoverObject) {
		//only handle input if owned by a human player and currently selected
		if(player.human && isSelected) {

			//something other than the ground is being hovered over
			if(hoverObject.name != "Ground" && hoverObject.name != "Fog") {

				Projectile pro = hoverObject.transform.root.GetComponent<Projectile>();
				if(pro) { return; }

				Player owner = hoverObject.transform.root.GetComponent<Player>();
				Unit unit = hoverObject.transform.GetComponentInParent<Unit>();
				Building building = hoverObject.transform.GetComponentInParent<Building>();

				if(owner) { //the object is owned by a player
					if(owner.username == player.username || owner.username == "Mother Nature"){
						player.hud.SetCursorState(CursorState.Select);
					} else if(CanAttack()){
						player.hud.SetCursorState(CursorState.Attack);
					} else {
						player.hud.SetCursorState(CursorState.Select);
					}
				} else if(unit || building && CanAttack()){
					player.hud.SetCursorState(CursorState.Attack);
				} else player.hud.SetCursorState(CursorState.Select);
			}
		}
	}

	/***********
	** PLAYER **
	***********/
	
	public bool IsOwnedBy(Player owner) {
		if(player && player.Equals(owner)) {
			return true;
		}
		return false;
	}
	
	/// <summary>
	/// Sets all of this WorldObject's children's Player component to the same as this WorldObject's Player component.
	/// </summary>
	public void SetPlayer() {
		player = transform.root.GetComponentInChildren<Player>();
	}

	public void SetPlayer(Player p) {
		player = p;
	}

	public Player getPlayer() {
		return player;
	}

	/************
	* MATERIALS *
	************/
	
	protected void SetTeamColor() {
		TeamColour[] teamColours = GetComponentsInChildren<TeamColour>();
		foreach(TeamColour teamColour in teamColours){
			teamColour.GetComponent<Renderer>().material.color = player.teamColour;
		}
	}
	
	public virtual void SetColliders(bool enabled) {
		Collider[] colliders = GetComponentsInChildren<Collider>();
		foreach(Collider collider in colliders){
			collider.enabled = enabled;
		}
	}
	 
	public void SetTransparentMaterial(Material material, bool storeExistingMaterial) {
		if(storeExistingMaterial) oldMaterials.Clear();
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		foreach(Renderer renderer in renderers) {
			if(storeExistingMaterial){
				oldMaterials.Add(renderer.material);
			}
			renderer.material = material;
		}
	}
	 
	public void RestoreMaterials() {
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		if(oldMaterials.Count == renderers.Length) {
			for(int i = 0; i < renderers.Length; i++) {
				renderers[i].material = oldMaterials[i];
			}
		}
	}
	 
	public void SetPlayingArea(Rect playingArea) {
		this.playingArea = playingArea;
	}


	/************
	* ATTACKING *
	************/

	public virtual bool UpdateTarget(WorldObject newTarget) {
		this.target = newTarget;
		return true;
	}


	public virtual bool CanAttack() {
		//default behaviour needs to be overidden by children
		return false;
	}

	public virtual bool ReadyToFire() {
		return this.currentWeaponChargeTime > this.weaponRechargeTime;
	}
	
	public bool TargetInWeaponRange() {
		Vector3 targetLocation = target.transform.position;
		Vector3 direction = targetLocation - transform.position;

		if(direction.sqrMagnitude < (weaponRange* weaponRangeModifier) * (weaponRange* weaponRangeModifier)) {
			return true;
		}
		return false;
	}
	
	protected virtual void UseWeapon() {
		currentWeaponChargeTime = 0.0f;
		//this behaviour needs to be specified by a specific object
	}


	protected bool FindNearbyEnemies() {
		nearbyObjects = WorkManager.FindNearbyObjects(transform.position, lineOfSight, player.human, this);

		if(nearbyObjects.Count > 0) {
			return true;
		}
		return false;
	}


	/**************
	** HITPOINTS **
	**************/

	public void TakeDamage(float damage, DamageType type) {
		
		float actualDamage = damage;

		if(type == DamageType.Hack) {
			actualDamage = damage * (1-hackArmour);
		} else if(type == DamageType.Piece) {
			actualDamage = damage * (1-pieceArmour);
		} else if(type == DamageType.Magic) {
			actualDamage = damage * (1-magicArmour);
		} else if(type == DamageType.Pure) { }
		
		hitPoints -= actualDamage;
		
		//Insert fancy death here
		if(hitPoints <= 0){
			Death();
		}
	}

	public void setHP(int hp) {
		hitPoints = hp;
		if(hitPoints <= 0) {
			Death();
		} else if(hitPoints > maxHitPoints) {
			hitPoints = maxHitPoints;
        }
	}

	public void setHPPercent(float hp) {
		hitPoints = maxHitPoints*hp;
		if(hitPoints <= 0) {
			Death();
		} else if(hitPoints > maxHitPoints) {
			hitPoints = maxHitPoints;
		}
	}

	public void Death() {
		if(PlayerSaveData.playerData.player.isObjectVisible(this)) {
			PlayerSaveData.playerData.player.removeWorldObjectVisisble(this);

			if(this.player != PlayerSaveData.playerData.player) {
				PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, GameControl.control.GetDeathGoldBounty(this));
			}
		}
		Destroy(this.gameObject);
	}

	/*******************
	** FOG VISIBILITY **
	*******************/

	public void setRenderers(bool visible) {
		this.visible = visible;
		foreach(Transform child in transform) {
			if(child != transform) {
				if(child.transform.gameObject.layer == LayerMask.NameToLayer("WorldObjects")) {
					child.transform.GetComponent<Renderer>().enabled = visible;
				}
			}
			
		}
	}

	/// <summary>
	/// This function handles what happens when this WorldObject collides with another object on the Default, Fog, BuildingPlacement or WorldObjects Layers.
	/// First a check to see if this WorldObject is owned by a human player is made, if it human owned the functions returns.
	/// Next, a check to see if this WorldObject has collided with Fog is made.
	/// If fog has been collided with and the fog is active then this WorldObject's renderers are turned off via setRenderers(bool).
	/// The object is also removed from the current human player's VisibleObjects list.
	/// </summary>
	/// <param name="other">The objects that this WorldObject has collided with.</param>
	public virtual void OnTriggerEnter(Collider other) {
		if(player != null && player.human) {
			return;
		}
		if(other.gameObject.layer == LayerMask.NameToLayer("Fog")) {
			hit = other.transform.GetComponent<Fog>();
			if(hit && hit.active) {
				//Debug.Log(objectName + " has hit some active fog");
				setRenderers(false);
				if(PlayerSaveData.playerData.player != null) {
					PlayerSaveData.playerData.player.removeWorldObjectVisisble(this);
				}
			} else if(hit && !hit.active) {
				//Debug.Log(objectName + " has hit some inactive fog");
				setRenderers(true);
			}
		}
	}

	/// <summary>
	/// This function handles what happens when this WorldObject collides with another object on the Default, Fog, BuildingPlacement or WorldObjects Layers.
	/// First a check to see if this WorldObject is owned by a human player is made, if it human owned the functions returns.
	/// Next, a check to see if this WorldObject has collided with Fog is made.
	/// If fog has been collided with and the fog is active then this WorldObject's renderers are turned off via setRenderers(bool).
	/// The object is also removed from the current human player's VisibleObjects list.
	/// Similarly if the fog collided with is not active then this WorldObject's renderers are turned on via setRenderers(bool).
	/// The object is then added to the current human player's VisibleObjects list.
	/// </summary>
	/// <param name="other">The objects that this WorldObject has collided with.</param>
	public virtual void OnTriggerStay(Collider other) {
		if(player != null && player.human) {
			return;
		}
		
		if(other.gameObject.layer == 8) {
			hit = other.transform.GetComponent<Fog>();
			if(hit && hit.active && this.visible) {
				setRenderers(false);
				PlayerSaveData.playerData.player.removeWorldObjectVisisble(this);
			} else if(hit && hit.active == false && this.visible == false) {
				setRenderers(true);
				PlayerSaveData.playerData.player.addWorldObjectToVisible(this);
			}
		}
	}

	public virtual void OnTriggerExit(Collider other) {
		if(other.gameObject.layer == 8) {
			hit = other.transform.GetComponent<Fog>();
			if(hit && hit.active == false && this.visible == false) {
				setRenderers(true);
				PlayerSaveData.playerData.player.addWorldObjectToVisible(this);
			}
		}
	}
}
