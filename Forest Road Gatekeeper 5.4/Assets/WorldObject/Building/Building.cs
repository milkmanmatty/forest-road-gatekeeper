using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RTS;

public class Building : WorldObject {
	
	public float maxBuildProgress;
	protected Queue<string> buildQueue;
	private float currentBuildProgress = 0.0f;
	private Vector3 spawnPoint;
	protected Vector3 rallyPoint;

	public virtual bool isRotateable { get; set; }
	public virtual bool upgradeable { get; set; }
	public int gameControlTowerID = -1;
	public int costBaseUpgrade = 25;
	public int costIncremenet = 25;

	public virtual bool canBuild { get; set; }

	private bool needsBuilding = false;
	public override bool IsActive { get { return !needsBuilding; } }
	private bool tempBuilding = false;
	public bool IsTempBuilding { get { return tempBuilding; } }

	public BuildingPlacement buildPlacer;
	public Bounds buildingPlacementBounds;
	
	public Texture2D rallyPointImage;
	public Texture2D sellImage;

	//Pathfinding Helper
	public Vector3[] baseContactLocations = new Vector3[8];
	private bool[] baseLocTaken = new bool[8];
	public NavMeshObstacle obstacle;

	//delayed updates.
	protected float timeBetweenDelayedUpdates = 0.5f;


	/******************
	** UNITY METHODS **
	******************/

	protected override void Awake() {
		base.Awake();
		buildQueue = new Queue<string>();
		float spawnX = selectionBounds.center.x + transform.forward.x * selectionBounds.extents.x + transform.forward.x * 5;
		float spawnZ = selectionBounds.center.z + transform.forward.z + selectionBounds.extents.z + transform.forward.z * 5;
		spawnPoint = new Vector3(spawnX, 0.0f, spawnZ);
		rallyPoint = spawnPoint;
		obstacle = this.GetComponentInChildren<NavMeshObstacle>();
	}
	 
	protected override void Start () {
		base.Start();
		this.isBuilding = true;
		buildingPlacementBounds = buildPlacer.GetComponent<Renderer>().bounds;
		BaseContactLocations_Update();

		StartCoroutine("DelayedUpdate");
	}

	protected override void Update() {
		base.Update();

		if(UnderConstruction() || tempBuilding) {
			return;
		}

		if(canBuild) {
			ProcessBuildQueue();
		}
	}

	IEnumerator DelayedUpdate() {
		while(true) {
			DecisionMaking();
			yield return new WaitForSeconds(timeBetweenDelayedUpdates);
		}
	}

	private void DecisionMaking() {

		if(UnderConstruction() || tempBuilding) {
			return;
		}

		//Building has a target
		if(this.target != null) {
			switch(this.state) {
				case UnitState.Idle:
					if(this.CanAttack()) {
						this.state = UnitState.Attacking;
					} else {
						this.target = null;
					}
					break;

				case UnitState.Attacking:
					if(this.TargetInWeaponRange()) {
						if(this.ReadyToFire()) {
							UseWeapon();
						}
						if(isRotateable) {
							transform.LookAt(target.transform);
						}
						//Otherwise just wait
					} else {
						this.state = UnitState.Idle;
						this.target = null;
					}
					break;
			}

		//Building doesn't have a target
		} else {
			switch(this.state) {
				case UnitState.Idle:
					if(this.CanAttack() && this.timeSinceLastScan > this.timeBetweenScans && FindNearbyEnemies()) {
						this.UpdateTarget(nearbyObjects[0]);
						this.state = UnitState.Attacking;
					}
					break;

				case UnitState.Attacking:
					this.state = UnitState.Idle;
					this.target = null;
					break;
			}
		}
	}


	protected override void OnGUI() {
		base.OnGUI();
		if(needsBuilding){
			DrawBuildProgress();
		}
	}

	void OnDrawGizmos() {
		for(int i = 0; i < baseContactLocations.Length; i++) {
			Gizmos.color = Color.green;
			if(this.baseLocTaken[i]) {
				Gizmos.color = Color.red;
			}
			Gizmos.DrawCube(baseContactLocations[i], Vector3.one);
		}
	}

	/**********************
	** BASE CONTACT LOCS **
	**********************/

	public void BaseContactLocations_Update(float multiplier = 1.5f, float diagonalmultiplier = 1.25f) {

		Bounds allBounds = buildingPlacementBounds;
		/*
        Renderer first = GetComponentInChildren<Renderer>();
		Bounds allBounds = first.bounds;

		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderers) {
			if(r != first) {
				allBounds.Encapsulate(r.bounds);
			}
		}*/
		
		//name is now misleading :/ should be halfSize
		Vector3 size = allBounds.extents;
		size += new Vector3(allBounds.extents.x/2f, 0, allBounds.extents.z/2f);
		Vector3 loc = transform.position;

		float m = multiplier;
		float dm = diagonalmultiplier;

		//Start at 12 o'clock and go clockwise
		this.baseContactLocations[0] = new Vector3(loc.x, 0, loc.z + (size.z * m));
		this.baseContactLocations[1] = new Vector3(loc.x + (size.x * dm), 0, loc.z + (size.z * dm));
		this.baseContactLocations[2] = new Vector3(loc.x + (size.x * m), 0, loc.z);
		this.baseContactLocations[3] = new Vector3(loc.x + (size.x * dm), 0, loc.z - (size.z * dm));

		//Half way, 6 O'clock
		this.baseContactLocations[4] = new Vector3(loc.x, 0, loc.z - (size.z * m));
		this.baseContactLocations[5] = new Vector3(loc.x - (size.x * dm), 0, loc.z - (size.z * dm));
		this.baseContactLocations[6] = new Vector3(loc.x - (size.x * m), 0, loc.z);
		this.baseContactLocations[7] = new Vector3(loc.x - (size.x * dm), 0, loc.z + (size.z * dm));

	}

	public Vector3 BaseLocation_ObtainFreeLoc() {
		for(int b = 0; b < baseLocTaken.Length; b++) {
			if(baseLocTaken[b] == false) {
				baseLocTaken[b] = true;
				return baseContactLocations[b];
			}
		}
		return ResourceManager.InvalidPosition;
	}

	public Vector3 BaseLocation_ObtainClosestFreeLoc(Vector3 loc) {

		float distance = 9999f;
		int locIndex = -1;

		for(int b = 0; b < baseLocTaken.Length; b++) {
			if(baseLocTaken[b] == false) {
				if(Vector3.Distance(baseContactLocations[b], loc) < distance) {
					distance = Vector3.Distance(baseContactLocations[b], loc);
					locIndex = b;
				}
			}
		}

		if(locIndex != -1) {
			Debug.Log(objectName + " [" + ObjID + "] has a free base location: " + locIndex);
			baseLocTaken[locIndex] = true;
			return baseContactLocations[locIndex];
		}

		return ResourceManager.InvalidPosition;
	}

	public int BaseLocation_ObtainClosestFreeLocID(Vector3 loc) {

		float distance = 9999f;
		int locIndex = -1;

		for(int b = 0; b < baseLocTaken.Length; b++) {
			if(baseLocTaken[b] == false) {
				if(Vector3.Distance(baseContactLocations[b], loc) < distance) {
					distance = Vector3.Distance(baseContactLocations[b], loc);
					locIndex = b;
				}
			}
		}

		if(locIndex != -1) {
			//Debug.Log(objectName + " [" + ObjID + "] has a free base location: " + locIndex);
			baseLocTaken[locIndex] = true;
			return locIndex;
		}

		return -1;
	}

	public bool BaseLocation_HasFreeLoc() {
		for(int b = 0; b < baseLocTaken.Length; b++) {
			if(baseLocTaken[b] == false) {
				return true;
			}
		}
		return false;
	}

	public void BaseLocation_ReleaseLoc(int id) {
		this.baseLocTaken[id] = false;
	}

	/************
	** UPGRADE **
	************/

	protected virtual void upgrade(Building up) {
		Debug.Log("upgrade "+objectName);
		GameControl.control.UpdateTowerLevelAndCosts(up);
    }


	/******************
	** UNIT CREATION **
	******************/

	protected void CreateUnit(string unitName) {
		buildQueue.Enqueue(unitName);
	}
	
	protected void ProcessBuildQueue() {
		if(buildQueue != null && buildQueue.Count > 0) {
			currentBuildProgress += Time.deltaTime * ResourceManager.BuildSpeed;
			if(currentBuildProgress > maxBuildProgress) {
				if(player){
					player.AddUnit(buildQueue.Dequeue(), spawnPoint, rallyPoint, transform.rotation, this);
				}
				currentBuildProgress = 0.0f;
			}
		}
	}
	
	public string[] getBuildQueueValues() {
		string[] values = new string[buildQueue.Count];
		int pos=0;
		foreach(string unit in buildQueue){
			values[pos++] = unit;
		}
		return values;
	}
	 
	/**************
	** SELECTION **
	**************/
	
	public override void SetSelection(bool selected, Player p, Rect playingArea) {
		base.SetSelection(selected, player, playingArea);
		if(player) {
			RallyPoint flag = player.GetComponentInChildren<RallyPoint>();
			if(selected) {
				if(flag && p.human && spawnPoint != ResourceManager.InvalidPosition && rallyPoint != ResourceManager.InvalidPosition) {
					flag.transform.localPosition = rallyPoint;
					flag.transform.forward = transform.forward;
					flag.Enable();
				}
			}
		}
	}

	public override void deselect() {
		base.deselect();
		if(hasSpawnPoint()) {
			RallyPoint flag = player.GetComponentInChildren<RallyPoint>();
			if(flag && player.human) {
				flag.Disable();
			}
		}
	}

	/********
	** HUD **
	********/
	
	public override void SetHoverState(GameObject hoverObject) {
		base.SetHoverState(hoverObject);
		//only handle input if owned by a human player and currently selected
		if(player.human && isSelected) {
			if(hoverObject.name == "Ground") {
				if(player.hud.GetPreviousCursorState() == CursorState.RallyPoint) player.hud.SetCursorState(CursorState.RallyPoint);
			}
		}
	}

	/***************
	** USER INPUT **
	***************/

	public override void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller) {
		base.MouseClick(hitObject, hitPoint, controller);
		//only handle input if owned by a human player and currently selected
		if(player.human && isSelected) {
			if(hitObject.name == "Ground") {
				if((player.hud.GetCursorState() == CursorState.RallyPoint || player.hud.GetPreviousCursorState() == CursorState.RallyPoint) && hitPoint != ResourceManager.InvalidPosition) {
					SetRallyPoint(hitPoint);
					player.hud.SetCursorState(CursorState.Move);
					player.hud.SetCursorState(CursorState.Select);
				}
			}
		}
	}


	/************************************
	* BUILDING PLACEMENT & CONSTRUCTION *
	************************************/

	/*
	public override void OnTriggerEnter(Collider other) {
		if(IsTempBuilding) {
			WorldObject hit = other.transform.GetComponent<WorldObject>();
			if(hit == null) {
				return;
			}
			if(hit.objectName == "Hideout") {
				Debug.Log("hit hideout");
			}
		}
	}*/

	public float getBuildPercentage() {
		return currentBuildProgress / maxBuildProgress;
	}

	public void setBuildingPlacement(BuildingPlacement bp) {
		this.buildPlacer = bp;
	}

	public bool canPlace() {
		if(buildPlacer == null) {
			Debug.LogError(this.objectName + " has no buildPlacer");
			return false;
		}
		return buildPlacer.canPlaceBuilding();
	}

	public bool UnderConstruction() {
		return needsBuilding;
	}

	public void Construct(int amount) {
		hitPoints += amount;
		if(hitPoints >= maxHitPoints) {
			hitPoints = maxHitPoints;
			needsBuilding = false;
			RestoreMaterials();
			SetTeamColor();
			obstacle.enabled = true;
		}
	}

	public void StartConstruction() {
		CalculateBounds();
		needsBuilding = true;
		hitPoints = 0;
		SetSpawnPoint();
	}

	private void DrawBuildProgress() {
		GUI.skin = ResourceManager.SelectBoxSkin;
		Rect selectBox = WorkManager.CalculateHealthBarPos(selectionBounds);
		selectBox.y = (playingArea.height - selectBox.y + ResourceManager.ORDERS_BAR_HEIGHT);

		//Draw the selection box around the currently selected object, within the bounds of the main draw area
		GUI.BeginGroup(playingArea);
		CalculateCurrentHealth(0.35f, 0.85f);
		DrawHealthBar(selectBox, "Building ...");
		GUI.EndGroup();
	}

	public override void SetColliders(bool enabled) {

		tempBuilding = !enabled;

		Collider[] colliders = GetComponentsInChildren<Collider>();
		foreach(Collider collider in colliders) {
			if(!collider.transform.GetComponent<BuildingPlacement>()) {
				collider.enabled = enabled;
			}
		}
	}

	/****************
	* SELL BUILDING *
	****************/

	public void Sell() {
		if(player) {
			float healthPercentage = (float) hitPoints / (float) maxHitPoints;
			int reimburse = Mathf.FloorToInt(sellValue * healthPercentage);
			player.AddResource(ResourceType.Gold, reimburse);
		}
		if(isSelected) {
			SetSelection(false, player, playingArea);
		}
		Destroy(this.gameObject);
	}

	public virtual bool canSell() {
		return true;
	}

	/**********************
	** SPAWN/RALLY POINT **
	**********************/

	public virtual bool hasSpawnPoint() {
		return spawnPoint != ResourceManager.InvalidPosition && rallyPoint != ResourceManager.InvalidPosition;
	}

	private void SetSpawnPoint() {
		float spawnX = selectionBounds.center.x + transform.forward.x * selectionBounds.extents.x + transform.forward.x * 5;
		float spawnZ = selectionBounds.center.z + transform.forward.z * selectionBounds.extents.z + transform.forward.z * 5;
		spawnPoint = new Vector3(spawnX, 0.0f, spawnZ);
		rallyPoint = spawnPoint;
	}

	public void SetRallyPoint(Vector3 position) {
		rallyPoint = position;
		if(player.human && isSelected) {
			RallyPoint flag = player.GetComponentInChildren<RallyPoint>();
			if(flag) {
				flag.transform.localPosition = rallyPoint;
			}
		}
	}

}
