using UnityEngine;
using System.Collections;

public class ShrineOfHealing : Building {

	public override bool upgradeable {
		get { return false; }
		set { base.upgradeable = false; }
	}

	protected override void Start () {
        base.Start();
        actions = new string[] { "Worker", "Tank" };
    }
	
	public override void PerformAction(string actionToPerform) {
		base.PerformAction(actionToPerform);
		CreateUnit(actionToPerform);
	}
}

