﻿using UnityEngine;
using System.Collections;
using RTS;

public class ArcherTower : Building {

	private static readonly int upHP = 25;
	private static readonly int upPDmg = 5;
	public static readonly int[] QUOTA_LEVELS = { 10, 15, 20, 30, 35 };

	public override bool upgradeable {
		get { return true; }
		set { }
	}

	public override bool isRotateable {
		get { return true; }
		set { }
	}

	protected override void Awake() {
		base.Awake();
	}

	protected override void Start() {
		base.Start();
		actions = new string[] { "Upgrade" };
		this.gameControlTowerID = GameControl.control.GetTowerIndex(this);
		int towerLevel = GameControl.control.towerLevels[gameControlTowerID];
		for(int up = 0; up < towerLevel; up++) {
			this.CheckForQuotaLevelUpgrade(up);
			this.upgrade();
		}
	}

	protected override void Update() {
		base.Update();
	}

	public override void PerformAction(string actionToPerform) {
		if(actionToPerform == "Upgrade") {

			int cost = GameControl.control.towerCosts[gameControlTowerID][0];

			if(player.GetResource(ResourceType.Gold) >= cost) {
				base.PerformAction(actionToPerform);
				player.AddResource(ResourceType.Gold, -cost);
				upgrade(this);
				CheckForQuotaLevelUpgrade(GameControl.control.towerLevels[gameControlTowerID]);
			} else {
				player.hud.setMessage("Not enough resources to upgrade", 5.0f);
			}
		}
	}

	protected override void upgrade(Building up) {
		base.upgrade(up);

		ArcherTower arch;

		foreach(GameObject gameKeep in GameObject.FindGameObjectsWithTag(this.tag)) {
			arch = gameKeep.GetComponent<ArcherTower>();
			arch.upgrade();
		}
	}

	public void upgrade() {
		this.AddHealth(upHP);
		this.AddAttack(0, upPDmg, 0);
	}

	private void CheckForQuotaLevelUpgrade(int lvl) {
		switch(lvl) {
			case (10):
				this.hackArmour += 0.05f;
				this.pieceArmour += 0.05f;
				this.AddAttack(0, 25, 0);
				break;
			case (15):
				this.hackArmour += 0.05f;
				this.pieceArmour += 0.05f;
				this.AddHealth(250);
				break;
			case (20):
				break;
			case (30):
				break;
			case (35):
				break;
		}
	}



	public override bool CanAttack() {
		return true;
	}

	protected override void UseWeapon() {
		base.UseWeapon();
		Vector3 spawnPoint = transform.position;
		spawnPoint.y += 1.5f;
		spawnPoint.x -= 0.5f;
		GameObject gameObject = (GameObject) Instantiate(ResourceManager.GetProjectile("Arrow"), spawnPoint, transform.rotation);
		Projectile projectile = gameObject.GetComponentInChildren<Projectile>();
		projectile.name = "Arrow";
		projectile.SetPlayer(player);
		projectile.SetDamage(DamageType.Hack, dmgHack);
		projectile.SetDamage(DamageType.Piece, this.dmgPiece);
		projectile.SetDamage(DamageType.Magic, dmgMagic);
		projectile.SetRange(weaponRange);
		projectile.SetTarget(target);
	}
}
