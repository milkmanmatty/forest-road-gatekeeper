﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RTS;

public class Keep : Building {

	private static readonly int upHP = 75;
	private static readonly int upPDmg = 3;
	private static readonly float upRange = 0.5f;
	public static readonly int[] QUOTA_LEVELS = { 10, 15, 20 };

	public override bool upgradeable {
		get { return true;	}
		set {}
	}
	public override bool isRotateable {
		get { return false; }
		set { }
	}

	protected override void Start() {
		base.Start();
		actions = new string[] { "Upgrade" };
		this.gameControlTowerID = GameControl.control.GetTowerIndex(this);
		int towerLevel = GameControl.control.towerLevels[gameControlTowerID];
        for(int up = 0; up < towerLevel; up++) {
			this.CheckForQuotaLevelUpgrade(up);
			this.upgrade();
		}
    }

	protected override void Update() {
		base.Update();
	}

	public override void PerformAction(string actionToPerform) {
		if(actionToPerform == "Upgrade") {

			int cost = GameControl.control.towerCosts[gameControlTowerID][0];

			if(player.GetResource(ResourceType.Gold) >= cost) {
				base.PerformAction(actionToPerform);
				player.AddResource(ResourceType.Gold, -cost);
				upgrade(this);
				CheckForQuotaLevelUpgrade(GameControl.control.towerLevels[gameControlTowerID]);
			} else {
				player.hud.setMessage("Not enough resources to upgrade", 5.0f);
			}
		}
	}

	protected override void upgrade(Building up) {
		base.upgrade(up);

		Keep keep;

		foreach(GameObject gameKeep in GameObject.FindGameObjectsWithTag(this.tag)) {
			keep = gameKeep.GetComponent<Keep>();
			keep.upgrade();
		}
	}

	public void upgrade() {
		this.AddHealth(upHP);
		this.AddAttack(0, upPDmg, 0);
		this.AddWeaponRange(upRange);
		if(GameControl.control.towerLevels[gameControlTowerID] % 2 == 0) {
			this.AddLoS(1);
		}
	}

	private void CheckForQuotaLevelUpgrade(int lvl) {
		switch(lvl) {
			case (10):
				this.AddHealth(800);
				this.hackArmour += 0.30f;
				this.AddAttack(0, 12, 0);
				break;
			case (15):
				this.AddHealth(1800);
				this.AddAttack(0, 120, 0);
				this.AddWeaponRange(2);
				break;
			case (20):
				this.AddHealth(3500);
				this.hackArmour += 0.20f;
				this.pieceArmour += 0.10f;
				this.AddAttack(0, 350, 0);
				break;
		}
	}

	public override bool CanAttack() {
		return true;
	}

	protected override void UseWeapon() {
		base.UseWeapon();
		Vector3 spawnPoint = transform.position;
		spawnPoint.y += 6f;
		GameObject gameObject = (GameObject) Instantiate(ResourceManager.GetProjectile("Arrow"), spawnPoint, transform.rotation);
		Projectile projectile = gameObject.GetComponentInChildren<Projectile>();
		projectile.name = "Arrow";
		projectile.SetPlayer(player);
		projectile.SetDamage(DamageType.Hack, dmgHack);
		projectile.SetDamage(DamageType.Piece, this.dmgPiece);
		projectile.SetDamage(DamageType.Magic, dmgMagic);
		projectile.SetRange(weaponRange);
		projectile.SetTarget(target);
	}
}
