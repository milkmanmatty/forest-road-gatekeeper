﻿using UnityEngine;
using System.Collections;

public class BuildingPlacement : MonoBehaviour {

	private bool canBuild = true;
	Building parent;

	void Awake () {
		parent = this.GetComponentInParent<Building>();
		if(!parent) {
			Debug.Log("BuildingPlacement has no parent object - destroying self");
			Destroy(this);
			return;
		}
		parent.setBuildingPlacement(this);
    }

	public void OnTriggerEnter(Collider other) {

		
		if(parent.IsTempBuilding) {
			GameObject obj = other.gameObject;

			if(!obj) {
				return;
			}

			string name = obj.name;
			//Debug.Log("BuildingPlacement colliding with " + name);

			if(name == "Fog") {
				Fog fogObj = obj.GetComponent<Fog>();
				if(fogObj && fogObj.seen == false) {
					canBuild = false;
					return;
				}
				return;
			} else if(name == "Hideout") {
				canBuild = true;
				return;
			}
			canBuild = false;
		}
    }

	public void OnTriggerStay(Collider other) {

		if(other.gameObject.layer == LayerMask.NameToLayer("Vision")) {
			return;
		}

		if(parent.IsTempBuilding) {
			GameObject obj = other.gameObject;

			if(!obj) {
				return;
			}

			string name = obj.name;

			if(name == "Fog") {
				Fog fogObj = obj.GetComponent<Fog>();
				if(fogObj && fogObj.seen == false) {
					canBuild = false;
					return;
				}
				return;
			} else if(name == "Hideout") {
				canBuild = true;
				return;
			}
			canBuild = false;
		}
	}

	public void OnTriggerExit(Collider other) {
		canBuild = true;
	}

	public bool canPlaceBuilding() {
		return canBuild;
	}

}
