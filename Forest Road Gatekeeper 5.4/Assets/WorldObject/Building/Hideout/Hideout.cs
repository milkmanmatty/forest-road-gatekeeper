using UnityEngine;
using System.Collections;
using RTS;

public class Hideout : Building {

	private static Hideout hideoutObject = null;
	private float percentDmg = 0;
	public float Hurt { get { return percentDmg; } }

	public override bool upgradeable {
		get { return false;	}
		set { base.upgradeable = false; }
	}

	protected override void Awake() {
		base.Awake();
		if(hideoutObject == null) {
			hideoutObject = this;
		} else {
			Debug.LogError("destroying self, hideoutObject == " + hideoutObject.ObjID);
			Destroy(gameObject);
			return;
		}
	}

	protected override void Start () {
        base.Start();
        actions = new string[] { "Basic Builder", "Improved Builder" };

		BaseContactLocations_Update(0.5f, 0.5f);
		percentDmg = this.maxHitPoints / 100.0f;
	}

	public static Hideout GetHideoutObject() {
		return hideoutObject;
	}
	
	public override void PerformAction(string actionToPerform) {
		base.PerformAction(actionToPerform);
		CreateUnit(actionToPerform);
	}
	
	public override bool canSell() {
		return false;
	}

}

