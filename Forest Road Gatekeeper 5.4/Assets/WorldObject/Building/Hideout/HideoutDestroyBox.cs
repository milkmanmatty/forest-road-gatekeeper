﻿using UnityEngine;
using System.Collections;
using RTS;

public class HideoutDestroyBox : MonoBehaviour {

	Hideout parent;

	void Start() {
		parent = GetComponentInParent<Hideout>();
		if(parent == null) {
			Destroy(this);
		}
	}

	public void OnTriggerEnter(Collider other) {
		WorldObject temp = other.transform.GetComponent<WorldObject>();
		if(temp == null) {
			return;
		}

		if(temp.getPlayer() != parent.getPlayer()) {
			parent.TakeDamage(Hideout.GetHideoutObject().Hurt, DamageType.Pure);
			Destroy(other.gameObject);
		}
	}
}
