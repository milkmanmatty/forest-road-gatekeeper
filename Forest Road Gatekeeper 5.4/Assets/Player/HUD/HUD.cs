using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RTS;
using System;

public class HUD : MonoBehaviour {

	public int localWave = 0;
	
	private int ORDERS_BAR_HEIGHT = ResourceManager.ORDERS_BAR_HEIGHT;
	private int RESOURCE_BAR_HEIGHT = ResourceManager.RESOURCE_BAR_HEIGHT;
	private int SELECTION_NAME_HEIGHT = ResourceManager.SELECTION_NAME_HEIGHT;
	
	public GUISkin resourceSkin;
	public GUISkin ordersSkin;
	public GUISkin selectBoxSkin;
	public GUISkin mouseCursorSkin;
	public GUISkin playerDetailsSkin;
	public GUISkin msgSkin;
	
	private string showMessageString = "";
	private float showMessageTime = 5.0f;
	private bool showMessageFlag = false;
	
	private const int ICON_WIDTH = 32;
	private const int ICON_HEIGHT = 32;
	private const int TEXT_WIDTH = 128;
	private const int TEXT_HEIGHT = 32;
	private const int HUDLINE_WIDTH = 25;
	private const int HUDLINE_HEIGHT = 100;
	private const int STATS_HEIGHT = 16;
	
	public Texture2D[] resources;
	private Dictionary<ResourceType, Texture2D> resourceImages;
	
	public Texture2D hudLine;
	public Texture2D upgradeButton;
	
	public Texture2D activeCursor;
	public Texture2D selectCursor;
	public Texture2D leftCursor;
	public Texture2D rightCursor;
	public Texture2D upCursor;
	public Texture2D downCursor;
	
	public Texture2D[] moveCursors;
	public Texture2D[] attackCursors;
	public Texture2D[] harvestCursors;
	
	private CursorState activeCursorState;
	private CursorState previousCursorState;
	private int currentFrame = 0;
	
	public Texture2D buttonHover;
	public Texture2D buttonClick;
	public Texture2D buildFrame;
	public Texture2D buildMask;
	
	public Texture2D smallButtonHover;
	public Texture2D smallButtonClick;
	public Texture2D rallyPointCursor;
	
	private int buildAreaHeight = 0;
	
	private const int BUILD_IMAGE_PADDING = 8;
	private const int BUILD_IMAGE_WIDTH = 64;
	private const int BUILD_IMAGE_HEIGHT = 64;
	private const int BUTTON_SPACING = 7;
	private const int SCROLL_BAR_WIDTH = 22;
	private const int GUI_COLUMN_MAX = 3;
	
	private Dictionary<ResourceType, int> resourceValues;
	private Dictionary<ResourceType, int> resourceLimits;
	
	private Player player;
	
	private WorldObject lastSelection;
	private float sliderValue;
	
	public Texture2D healthy;
	public Texture2D damaged;
	public Texture2D critical;
		
	public Texture2D[] resourceHealthBars;

	// Use this for initialization
	void Start () {
		resourceValues = new Dictionary<ResourceType, int>();
		resourceLimits = new Dictionary<ResourceType, int>();
		player = transform.root.GetComponent<Player>();
		ResourceManager.StoreSelectBoxItems(selectBoxSkin, healthy, damaged, critical);
		SetCursorState(CursorState.Select);
		resourceImages = new Dictionary<ResourceType, Texture2D>();
		for(int i = 0; i < resources.Length; i++) {
			switch(resources[i].name) {
				case "Gold":
					resourceImages.Add(ResourceType.Gold, resources[i]);
					resourceValues.Add(ResourceType.Gold, 0);
					resourceLimits.Add(ResourceType.Gold, 0);
					break;
				case "DivinePower":
					resourceImages.Add(ResourceType.Power, resources[i]);
					resourceValues.Add(ResourceType.Power, 0);
					resourceLimits.Add(ResourceType.Power, 0);
					break;
				default: break;
			}
		}
		
		buildAreaHeight = ORDERS_BAR_HEIGHT;
		
		Dictionary<ResourceType, Texture2D> resourceHealthBarTextures = new Dictionary<ResourceType, Texture2D>();
		ResourceManager.SetResourceHealthBarTextures(resourceHealthBarTextures);
	}
	
	void OnGUI() {
		if(player && player.human) {
			DrawOrdersBar();
			//DrawPlayerDetails();
			DrawResourceBar();
			DrawMouseCursor();
			if(showMessageString != ""){
				if(!showMessageFlag){
					StartCoroutine(startMessageTimer());
				}
				showMessage();
			}
		}
	}
	
	
	private void DrawPlayerDetails() {
		GUI.skin = playerDetailsSkin;
		GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
		
		float height = ResourceManager.TextHeight;
		float leftPos = ResourceManager.Padding;
		float topPos = Screen.height - height - ResourceManager.Padding;
		
		Texture2D avatar = PlayerSaveData.playerData.getPlayerAvatar();
		if(avatar) {
			//we want the texture to be drawn square at all times
			GUI.DrawTexture(new Rect(leftPos, topPos, height, height), avatar);
			leftPos += height + ResourceManager.Padding;
		}
		
		float minWidth = 0, maxWidth = 0;
		string playerName = PlayerSaveData.playerData.getPlayerName();
		playerDetailsSkin.GetStyle("label").CalcMinMaxWidth(new GUIContent(playerName), out minWidth, out maxWidth);
		GUI.Label(new Rect(leftPos, topPos, maxWidth, height), playerName);
		
		GUI.EndGroup();
	}
	
	
	//Screen coordinates start in the lower-left corner of the screen
    //not the top-left of the screen like the drawing coordinates do
	public bool MouseInBounds() {
		Vector3 mousePos = Input.mousePosition;
		bool insideHeight = mousePos.y > ORDERS_BAR_HEIGHT && mousePos.y <= Screen.height - RESOURCE_BAR_HEIGHT;
		return insideHeight;
	}
	
	private void DrawOrdersBar() {
		GUI.skin = ordersSkin;
		GUI.BeginGroup(new Rect(0,Screen.height-ORDERS_BAR_HEIGHT,Screen.width,ORDERS_BAR_HEIGHT));
		GUI.Box(new Rect(0,0,Screen.width,ORDERS_BAR_HEIGHT),"");
		
		string selectionName = "";
		Texture2D buildImg = null;
		if(player.SelectedObjects.Count > 0) {
            WorldObject lastSelected = player.SelectedObjects.Last.Value;
			if(lastSelected != null) {
				selectionName = lastSelected.objectName;
				buildImg = lastSelected.buildImage;
				if(lastSelected.IsOwnedBy(player)) {

					//reset slider value if the selected object has changed
					if(lastSelection && lastSelection != lastSelected) {
						sliderValue = 0.0f;
					}
					if(lastSelected.IsActive) {
						DrawActions(lastSelected.GetActions());
					}

					//store the current selection
					lastSelection = lastSelected;
					Building selectedBuilding = lastSelection.GetComponent<Building>();
					if(selectedBuilding) {
						DrawBuildQueue(selectedBuilding.getBuildQueueValues(), selectedBuilding.getBuildPercentage());
						DrawStandardBuildingOptions(selectedBuilding);
					}

				}
				DisplayUnitStats(lastSelected);
			}
		}
		if(!selectionName.Equals("")) {
			//int topPos = buildAreaHeight + BUTTON_SPACING;
			GUI.Label(new Rect(Screen.width/2 - TEXT_WIDTH/2, BUTTON_SPACING, TEXT_WIDTH, SELECTION_NAME_HEIGHT), selectionName);
		}
		if(buildImg) {
			GUI.DrawTexture(new Rect(Screen.width/2 - BUILD_IMAGE_WIDTH/2, TEXT_HEIGHT + BUILD_IMAGE_PADDING, BUILD_IMAGE_WIDTH, BUILD_IMAGE_HEIGHT), buildImg);
		}
		
		
		DrawHudLines();
		GUI.EndGroup();
	}
	
	private void DrawResourceBar() {
		GUI.skin = resourceSkin;
		GUI.BeginGroup(new Rect(0,0,Screen.width,RESOURCE_BAR_HEIGHT));
		GUI.Box(new Rect(0,0,Screen.width,RESOURCE_BAR_HEIGHT),"");
		int topPos = 0;
		int iconLeft = 4;
		int textLeft = 4;
		DrawResourceIcon(ResourceType.Gold, iconLeft, textLeft, topPos);
		iconLeft += TEXT_WIDTH;
		textLeft += TEXT_WIDTH;
		DrawResourceIcon(ResourceType.Power, iconLeft, textLeft, topPos);

		GUI.Label(new Rect(Screen.width / 2, BUTTON_SPACING, TEXT_WIDTH, SELECTION_NAME_HEIGHT), "Wave: "+localWave);

		int padding = 7;
		int buttonWidth = padding + (TEXT_WIDTH/2);
		int buttonHeight = RESOURCE_BAR_HEIGHT - 2 * padding;
		int leftPos = Screen.width - buttonWidth;
		Rect menuButtonPosition = new Rect(leftPos, padding, buttonWidth, buttonHeight);
		 
		if(GUI.Button(menuButtonPosition, "Menu")) {
			Time.timeScale = 0.0f;
			PauseMenu pauseMenu = GetComponent<PauseMenu>();
			if(pauseMenu){
				pauseMenu.enabled = true;
			}
			UserInput userInput = player.GetComponent<UserInput>();
			if(userInput){
				userInput.enabled = false;
			}
		}
		
		GUI.EndGroup();
	}
	
	public Rect GetPlayingArea() {
		return new Rect(0, RESOURCE_BAR_HEIGHT, Screen.width, Screen.height - RESOURCE_BAR_HEIGHT - ORDERS_BAR_HEIGHT);
		//Screen.height - RESOURCE_BAR_HEIGHT
	}
	
	/**********************/
	/*** CURSOR DISPLAY ***/
	/**********************/
	
	private void DrawMouseCursor() {
		bool mouseOverHud = !MouseInBounds() && activeCursorState != CursorState.PanDown && activeCursorState != CursorState.PanUp;
		if(mouseOverHud || ResourceManager.MenuOpen) {
			Cursor.visible = true;
		} else {
			Cursor.visible = false;
			if(!player.IsFindingBuildingLocation()) {
				GUI.skin = mouseCursorSkin;
				GUI.BeginGroup(new Rect(0,0,Screen.width,Screen.height));
				UpdateCursorAnimation();
				Rect cursorPosition = GetCursorDrawPosition();
				GUI.Label(cursorPosition, activeCursor);
				GUI.EndGroup();
			}
		}
	}
	
	private void UpdateCursorAnimation() {
		//sequence animation for cursor (based on more than one image for the cursor)
		//change once per second, loops through array of images
		if(activeCursorState == CursorState.Move) {
			currentFrame = (int)Time.time % moveCursors.Length;
			activeCursor = moveCursors[currentFrame];
		} else if(activeCursorState == CursorState.Attack) {
			currentFrame = (int)Time.time % attackCursors.Length;
			activeCursor = attackCursors[currentFrame];
		} else if(activeCursorState == CursorState.Harvest) {
			currentFrame = (int)Time.time % harvestCursors.Length;
			activeCursor = harvestCursors[currentFrame];
		}
	}
	private Rect GetCursorDrawPosition() {
		//set base position for custom cursor image
		float leftPos = Input.mousePosition.x;
		float topPos = Screen.height - Input.mousePosition.y; //screen draw coordinates are inverted
		//adjust position base on the type of cursor being shown
		if(activeCursorState == CursorState.PanRight){
			leftPos = Screen.width - activeCursor.width;
		} else if(activeCursorState == CursorState.PanDown){
			topPos = Screen.height - activeCursor.height;
		} else if(activeCursorState == CursorState.Move || activeCursorState == CursorState.Select || activeCursorState == CursorState.Harvest) {
			topPos -= activeCursor.height / 2;
			leftPos -= activeCursor.width / 2;
		} else if(activeCursorState == CursorState.RallyPoint){
			topPos -= activeCursor.height;
		}
		return new Rect(leftPos, topPos, activeCursor.width, activeCursor.height);
	}
	
	public CursorState GetPreviousCursorState() {
		return previousCursorState;
	}
	
	public void SetCursorState(CursorState newState) {
		if(activeCursorState != newState){
			previousCursorState = activeCursorState;
		}
		activeCursorState = newState;
		switch(newState) {
		case CursorState.Select:
			activeCursor = selectCursor;
			break;
		case CursorState.Attack:
			currentFrame = (int)Time.time % attackCursors.Length;
			activeCursor = attackCursors[currentFrame];
			break;
		case CursorState.Harvest:
			currentFrame = (int)Time.time % harvestCursors.Length;
			activeCursor = harvestCursors[currentFrame];
			break;
		case CursorState.Move:
			currentFrame = (int)Time.time % moveCursors.Length;
			activeCursor = moveCursors[currentFrame];
			break;
		case CursorState.PanLeft:
			activeCursor = leftCursor;
		break;
		case CursorState.PanRight:
			activeCursor = rightCursor;
			break;
		case CursorState.PanUp:
			activeCursor = upCursor;
			break;
		case CursorState.PanDown:
			activeCursor = downCursor;
			break;
		case CursorState.RallyPoint:
			activeCursor = rallyPointCursor;
			break;
		default: break;
		}
	}
	
	public CursorState GetCursorState() {
		return activeCursorState;
	}
	
	/********************/
	/* RESOURCE DISPLAY */
	/********************/
	
	public void SetResourceValues(Dictionary<ResourceType, int> resourceValues, Dictionary<ResourceType, int> resourceLimits) {
		this.resourceValues = resourceValues;
		this.resourceLimits = resourceLimits;
	}
	
	private void DrawResourceIcon(ResourceType type, int iconLeft, int textLeft, int topPos) {
		Texture2D icon = resourceImages[type];
		string text = resourceValues[type].ToString() + "/" + resourceLimits[type].ToString();
		GUI.DrawTexture(new Rect(iconLeft, topPos, ICON_WIDTH, ICON_HEIGHT), icon);
		GUI.Label (new Rect(textLeft+ICON_WIDTH, topPos, TEXT_WIDTH, TEXT_HEIGHT), text);
	}
	
	private void DrawHudLines(){
		/* Selected Unit Portrait */
		GUI.DrawTexture(new Rect(Screen.width/2 - TEXT_WIDTH/2 - 25, 0, HUDLINE_WIDTH, ORDERS_BAR_HEIGHT), hudLine);
		GUI.DrawTexture(new Rect(Screen.width/2 + TEXT_WIDTH/2 + 2, 0, HUDLINE_WIDTH, ORDERS_BAR_HEIGHT), hudLine);
		
		/* Selected Unit Stats */
		GUI.DrawTexture(new Rect(Screen.width/2 - (TEXT_WIDTH*2 + TEXT_WIDTH/2) - 25, 0, HUDLINE_WIDTH, ORDERS_BAR_HEIGHT), hudLine);
		
		/* Selected Unit Build Actions */
		GUI.DrawTexture(new Rect((BUILD_IMAGE_WIDTH * 6) + (BUILD_IMAGE_WIDTH/2) - 25, 0, HUDLINE_WIDTH, ORDERS_BAR_HEIGHT), hudLine);
		;
		
		/* Selected Unit Ability Actions */
		GUI.DrawTexture(new Rect(Screen.width - BUILD_IMAGE_WIDTH - BUILD_IMAGE_PADDING - 25, 0, HUDLINE_WIDTH, ORDERS_BAR_HEIGHT), hudLine);
	}
	
	/*******************/
	/*** UNITS STATS ***/
	/*******************/
	
	private void DisplayUnitStats(WorldObject obj){
		string hp = "Hit Points: "+Math.Round(obj.hitPoints, 1, MidpointRounding.AwayFromZero) +"/"+obj.maxHitPoints;
		
		string hDmg = "Hack Damage: "+obj.dmgHack;
		string pDmg = "Piece Damage: "+obj.dmgPiece;
		string mDmg = "Magic Damage: "+obj.dmgMagic;
		
		string range = "Range: "+ Math.Round(obj.EffectiveRange, 1, MidpointRounding.AwayFromZero);
		string hArm = "Hack Armour: "+(100 * obj.hackArmour)+"%";
		string pArm = "Piece Armour: "+(100 * obj.pieceArmour)+"%";
		string mArm = "Magic Shield: "+(100 * obj.magicArmour)+"%";

		string speed = "";
		Unit unit = obj.GetComponent<Unit>();
		if(unit) {
			speed = "Speed: " + unit.moveSpeed;
		}
		
		string sell = "";
		string upCost = "";
		Building building = obj.GetComponent<Building>();
		if(building){
			sell = "Sell Value: "+obj.sellValue;
            if(building.upgradeable) {
				upCost = "Upgrade Cost: " + GameControl.control.towerCosts[building.gameControlTowerID][0];
			}
		}
		
		int h = STATS_HEIGHT;
		int left = Screen.width/2 - (TEXT_WIDTH*2 + TEXT_WIDTH/2) - 10;
		int right = Screen.width/2 - (TEXT_WIDTH + TEXT_WIDTH/2) - 15;
		int mid = Screen.width/2 - (TEXT_WIDTH*2) - 12;
		
		GUI.Label (new Rect(mid, 10+0*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), hp);
		GUI.Label (new Rect(left, h+1*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), range);
		if(unit) {
			GUI.Label(new Rect(right, h + 1 * STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), speed);
		}
		
		GUI.Label (new Rect(left, h+3*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), hDmg);
		GUI.Label (new Rect(left, h+4*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), pDmg);
		GUI.Label (new Rect(left, h+5*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), mDmg);
		
		GUI.Label (new Rect(right, h+3*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), hArm);
		GUI.Label (new Rect(right, h+4*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), pArm);
		GUI.Label (new Rect(right, h+5*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), mArm);
		
		GUI.Label (new Rect(mid, h+6*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), sell);

		GUI.Label(new Rect(Screen.width/2 - TEXT_WIDTH/2, h/2 + 6*STATS_HEIGHT, TEXT_WIDTH, STATS_HEIGHT), upCost);
	}
	
	/***************/
	/*** ACTIONS ***/
	/***************/
	
	private void DrawActions(string[] actions) {
		GUIStyle buttons = new GUIStyle();
		buttons.hover.background = buttonHover;
		buttons.active.background = buttonClick;
		GUI.skin.button = buttons;
		
		int numActions = actions.Length;
		//define the area to draw the actions inside
		GUI.BeginGroup(new Rect(0, 0, Screen.width, buildAreaHeight));
		
		int row = 0;
		
		//display possible actions as buttons and handle the button click for each
		for(int i = 0; i < numActions; i++) {
			int column = i % GUI_COLUMN_MAX;
			if(i > 0 && i % GUI_COLUMN_MAX == 0){
				row++;
			}
			Rect pos = GetButtonPos(row, column);


			if(actions[i] == "Upgrade") {
				pos = GetButtonPos(row, column);
				if(GUI.Button(pos, upgradeButton)) {
					if(player.SelectedObjects.Count > 0) {
						player.SelectedObjects.Last.Value.PerformAction(actions[i]);
						continue;
					}
				}
			}

			Texture2D action = ResourceManager.GetBuildImage(actions[i]);
            if(action) {
				//create the button and handle the click of that button
				if(GUI.Button(pos, action)){
					if(player.SelectedObjects.Count > 0) {
						player.SelectedObjects.Last.Value.PerformAction(actions[i]);
					}
				}
			}
		}
		GUI.EndGroup();
	}

	private int MaxNumRows(int areaHeight) {
		Debug.Log("MaxNumRows: "+ (areaHeight / BUILD_IMAGE_HEIGHT));
		return areaHeight / BUILD_IMAGE_HEIGHT;
	}
	 
	private Rect GetButtonPos(int row, int column, bool half = false) {
		int left = SCROLL_BAR_WIDTH + column * BUILD_IMAGE_WIDTH;
		float top = row * BUILD_IMAGE_HEIGHT - sliderValue * BUILD_IMAGE_HEIGHT;
		if(half) {
			return new Rect(left, top, BUILD_IMAGE_WIDTH/2, BUILD_IMAGE_HEIGHT/2);
		}
		return new Rect(left, top, BUILD_IMAGE_WIDTH, BUILD_IMAGE_HEIGHT);
	}
	
	private void DrawBuildQueue(string[] buildQueue, float buildPercentage) {
		for(int i = 0; i < buildQueue.Length; i++) {
			
			float topPos = BUILD_IMAGE_PADDING / 2;
			
			int retainer = (BUILD_IMAGE_WIDTH * 6) + (BUILD_IMAGE_WIDTH/2);
			int buildImgSize = BUILD_IMAGE_WIDTH + BUILD_IMAGE_PADDING;
			float lengthPos = retainer + ((GUI_COLUMN_MAX + i)%3) * buildImgSize;
			if(i > 2){
				topPos += BUILD_IMAGE_WIDTH;
			}
			
			Rect buildPos = new Rect(lengthPos, topPos, BUILD_IMAGE_WIDTH, BUILD_IMAGE_HEIGHT);
			
			GUI.DrawTexture(buildPos, ResourceManager.GetBuildImage(buildQueue[i]));
			
			GUI.DrawTexture(buildPos, buildFrame);
			
			topPos += BUILD_IMAGE_PADDING;
			
			float width = BUILD_IMAGE_WIDTH - 2 * BUILD_IMAGE_PADDING;
			float height = BUILD_IMAGE_HEIGHT - 2 * BUILD_IMAGE_PADDING;
			
			if(i==0) {
				//shrink the build mask on the item currently being built to give an idea of progress
				topPos += height * buildPercentage;
				height *= (1 - buildPercentage);
			}
			GUI.DrawTexture(new Rect(lengthPos + BUILD_IMAGE_PADDING, topPos, width, height), buildMask);
		}
	}
	
	private void DrawStandardBuildingOptions(Building building) {
		GUIStyle buttons = new GUIStyle();
		buttons.hover.background = smallButtonHover;
		buttons.active.background = smallButtonClick;
		GUI.skin.button = buttons;
		int leftPos = Screen.width - BUILD_IMAGE_WIDTH - BUILD_IMAGE_PADDING;
		int topPos = BUTTON_SPACING;
		int width = BUILD_IMAGE_WIDTH / 2;
		int height = BUILD_IMAGE_HEIGHT / 2;
		
		Rect sellButton = new Rect(leftPos, topPos, width, height);
		
		if(building.canSell()){
			if(GUI.Button(sellButton, building.sellImage)) {
				building.Sell();
			}
		}
		
		if(building.hasSpawnPoint()) {
			Rect rallyPointButton = new Rect(leftPos, topPos + BUILD_IMAGE_PADDING + (BUILD_IMAGE_HEIGHT / 2), width, height);
			if(GUI.Button(rallyPointButton, building.rallyPointImage)){
				if(activeCursorState != CursorState.RallyPoint && previousCursorState != CursorState.RallyPoint) {
					SetCursorState(CursorState.RallyPoint);
				} else {
					//dirty hack to ensure toggle between RallyPoint and not works ...
					SetCursorState(CursorState.Move);
					SetCursorState(CursorState.Select);
				}
			}
		}
	}
	
	public void setMessage(string msg, float displayTime){
		showMessageString = msg;
		showMessageTime = displayTime;
	}
	
	protected void showMessage(){
		GUI.skin = msgSkin;
		int msgLength = showMessageString.Length * 10;
		GUI.Label (new Rect(Screen.width/2 - msgLength/2, Screen.height/2 - TEXT_HEIGHT/2, msgLength, TEXT_HEIGHT), showMessageString);
	}
	
	IEnumerator startMessageTimer(){
		showMessageFlag = true;
		yield return new WaitForSeconds (showMessageTime);
		removeMessage();
	}
	
	private void removeMessage(){
		showMessageString = "";
		showMessageTime = 0f;
		showMessageFlag = false;
	}
	
}