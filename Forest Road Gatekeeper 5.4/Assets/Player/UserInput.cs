using UnityEngine;
using System.Collections;
using RTS;

public class UserInput : MonoBehaviour {
		
	private Player player;
    private bool isSelecting = false;
    private Vector3 initialMousePoint;

	// Use this for initialization
	void Start () {
		player = transform.root.GetComponent<Player>();
		if(player == null) {
			Debug.LogError("UserInput's Player is null");
		}
		if(player.hud == null) {
			Debug.Log("[UserInput] " + player.username+" has no hud! Attempting to reassign");
			player.hud = this.GetComponentInChildren<HUD>();
			if(player.hud == null) {
				Debug.LogError("[UserInput] " + player.username + " still null!");
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(player.human) {
			if(Input.GetKeyDown(KeyCode.Escape)){
				OpenPauseMenu();
			}
			MoveCamera();
			RotateCamera();
			MouseActivity();
		}
	}

    void OnGUI() {
        if (isSelecting) {
            // Create a rect from both mouse positions
            var rect = ResourceManager.GetScreenRect(initialMousePoint, Input.mousePosition);
            ResourceManager.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            ResourceManager.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    private void OpenPauseMenu() {
		Time.timeScale = 0.0f;
		GetComponentInChildren<PauseMenu>().enabled = true;
		GetComponent<UserInput>().enabled = false;
		Cursor.visible = true;
		ResourceManager.MenuOpen = true;
	}
	
	private void MoveCamera() {
		float xpos = Input.mousePosition.x;
		float ypos = Input.mousePosition.y;
		Vector3 movement = new Vector3(0,0,0);
		bool mouseScroll = false;
		
		//away from ground movement
		if(Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.Equals)) {
			ZoomCamera();
			return;
		}
 
		//horizontal camera movement
		if((xpos >= 0 && xpos < ResourceManager.ScrollWidth) || Input.GetKey(KeyCode.LeftArrow)) {
			if(xpos >= 0 && xpos < ResourceManager.ScrollWidth) {
				player.hud.SetCursorState(CursorState.PanLeft);
			}
			movement.x -= ResourceManager.ScrollSpeed;
			mouseScroll = true;
		} else if((xpos <= Screen.width && xpos > Screen.width - ResourceManager.ScrollWidth) || Input.GetKey(KeyCode.RightArrow)) {
			if(xpos <= Screen.width && xpos > Screen.width - ResourceManager.ScrollWidth){
				player.hud.SetCursorState(CursorState.PanRight);
			}
			movement.x += ResourceManager.ScrollSpeed;
			mouseScroll = true;
		}
		 
		//vertical camera movement
		if((ypos >= 0 && ypos < ResourceManager.ScrollWidth) || Input.GetKey(KeyCode.DownArrow)) {
			if(ypos >= 0 && ypos < ResourceManager.ScrollWidth){
				player.hud.SetCursorState(CursorState.PanDown);
			}
			movement.z -= ResourceManager.ScrollSpeed;
			mouseScroll = true;
		} else if((ypos <= Screen.height && ypos > Screen.height - ResourceManager.ScrollWidth) || Input.GetKey(KeyCode.UpArrow)) {
			if(ypos <= Screen.height && ypos > Screen.height - ResourceManager.ScrollWidth){
				player.hud.SetCursorState(CursorState.PanUp);
			}
			movement.z += ResourceManager.ScrollSpeed;
			mouseScroll = true;
		}
		
		//make sure movement is in the direction the camera is pointing
		//but ignore the vertical tilt of the camera to get sensible scrolling
		movement = Camera.main.transform.TransformDirection(movement);
		movement.y = 0;
		
		//calculate desired camera position based on received input
		Vector3 origin = Camera.main.transform.position;
		Vector3 destination = origin;
		destination.x += movement.x;
		destination.y += movement.y;
		destination.z += movement.z;
		
		//limit away from ground movement to be between a minimum and maximum distance
		if(destination.y > ResourceManager.MaxCameraHeight) {
			destination.y = ResourceManager.MaxCameraHeight;
		} else if(destination.y < ResourceManager.MinCameraHeight) {
			destination.y = ResourceManager.MinCameraHeight;
		}
		
		//if a change in position is detected perform the necessary update
		if(destination != origin) {
			Camera.main.transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime * ResourceManager.ScrollSpeed);
		}
		
		if(!mouseScroll) {
			player.hud.SetCursorState(CursorState.Select);
		}
	}
	
	private void ZoomCamera(){
		
		Vector3 origin = Camera.main.transform.position;
		
				
		if(Input.GetKey(KeyCode.Minus) && origin.y < ResourceManager.MaxCameraHeight) {
			Camera.main.transform.Translate(Vector3.back * 0.1f * ResourceManager.ZoomSpeed);
		} else if(Input.GetKey(KeyCode.Equals) && origin.y > ResourceManager.MinCameraHeight) {
			Camera.main.transform.Translate(Vector3.forward * 0.1f * ResourceManager.ZoomSpeed);
			
		}
	}
	 
	private void RotateCamera() {
		Vector3 origin = Camera.main.transform.eulerAngles;
		Vector3 destination = origin;
		Vector3 rotPoint = new Vector3();
		 
		destination.z -= ResourceManager.RotateAmount * Input.GetAxis("Mouse ScrollWheel");

		Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 900)) {
			rotPoint = hit.point;
		}

		//if a change in position is detected perform the necessary update
		if(destination != origin) {
			if(destination.z > origin.z){
				Camera.main.transform.RotateAround(rotPoint, Vector3.up, ResourceManager.RotateAmount);
			} else if(destination.z < origin.z){
				Camera.main.transform.RotateAround(rotPoint, Vector3.down, ResourceManager.RotateAmount);
			}
		}
	}

    private void MouseActivity() {
        if (Input.GetMouseButtonDown(0)) {
            isSelecting = true;
            initialMousePoint = Input.mousePosition;
            LeftMouseClick();
        } else if (Input.GetMouseButtonDown(1)) {
            RightMouseClick();
        } else if (Input.GetMouseButtonUp(0)) {
            isSelecting = false;
        }
        MouseHover();
	}
	
	private void LeftMouseClick() {
		if(player.hud.MouseInBounds()) {
			if(player.IsFindingBuildingLocation()) {
				if(player.getTempBuilding().canPlace()) {
					player.StartConstruction();
				}
			} else {
				GameObject hitObject = WorkManager.FindHitObject(Input.mousePosition);

				if(hitObject && hitObject.layer == LayerMask.NameToLayer("WorldObjects")) {
					WorldObject worldObject = hitObject.transform.GetComponentInParent<WorldObject>();
					if(worldObject) {

						if(!(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))) {
							player.removeAllSelected();
						}

						worldObject.SetSelection(true, player, player.hud.GetPlayingArea());
					} else {
						Debug.Log(hitObject.name+" has no WorldObject script");
					}
				}
				
				//If the player has clicked on the ground -- deselect
				else {
					if(player.SelectedObjects.Count > 0) {
						player.removeAllSelected();
					}
                }
			}
		}
	}
	
	private void RightMouseClick() {
		if(player.hud.MouseInBounds()){
			if(player.SelectedObjects.Count > 0) {
				if(player.IsFindingBuildingLocation()) {
					player.CancelBuildingPlacement();
				} else {
					GameObject hitObject = WorkManager.FindHitObject(Input.mousePosition);
					Vector3 hitPoint = WorkManager.FindHitPoint(Input.mousePosition);
					foreach (WorldObject selectedUnit in player.SelectedObjects) {
						selectedUnit.MouseClick(hitObject, hitPoint, player);
					}
				}
			}
		}
	}
	
	private void MouseHover() {
		if(player.hud.MouseInBounds()) {
			if(player.IsFindingBuildingLocation()) {
				player.FindBuildingLocation();
			} else {

				GameObject hoverObject = WorkManager.FindHitObject(Input.mousePosition);
				if(hoverObject) {

					if(player.SelectedObjects.Count > 0) {
						player.SelectedObjects.Last.Value.SetHoverState(hoverObject);
					} else if(hoverObject.name != "Ground") {
						Player owner = hoverObject.transform.root.GetComponent<Player>();
						if(owner) {
							Unit unit = hoverObject.transform.parent.GetComponent<Unit>();
							Building building = hoverObject.transform.parent.GetComponent<Building>();
							if(owner.username == player.username && (unit || building)) player.hud.SetCursorState(CursorState.Select);
						}
					}
				}
			}
		}
	}

    public bool IsWithinSelectionBounds(GameObject gameObject) {
        if (!isSelecting) {
            return false;
        }

        var camera = Camera.main;
        var viewportBounds = ResourceManager.GetViewportBounds(camera, initialMousePoint, Input.mousePosition);

        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }


    private void checkAllUnits() {
        object[] obj = Object.FindObjectsOfType(typeof(WorldObject));
        foreach (object o in obj) {
            WorldObject g = (WorldObject) o;
            Debug.Log(g.name);
        }
    }

	public void setPlayer(Player p) {
		this.player = p;
		Debug.Log("[UserInput] Player has been set to: "+p.username);
	}
    


}





































