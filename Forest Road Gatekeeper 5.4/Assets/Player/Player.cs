using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using RTS;

public class Player : MonoBehaviour {
	
	public HUD hud;
    public bool human;
	public string username;
	public Color teamColour;

	public int populationCount = 0;
	
	public int startGold = 250;
	public int startGoldLimit = 1000000;
	public int startPower = 100;
	public int startPowerLimit = 100;
	
	private Dictionary<ResourceType, int> resources;
	private Dictionary<ResourceType, int> resourceLimits;

    public LinkedList<WorldObject> VisibleObjects;
    public LinkedList<WorldObject> SelectedObjects;

    public Material notAllowedMaterial;
	public Material allowedMaterial;
 
	private Building tempBuilding = null;
	private Unit tempCreator;
	private bool findingPlacement = false;

	private bool runOnce = true;

	public void setDataToPlayerSaveData() {
		username = PlayerSaveData.playerData.getPlayerName();
		PlayerSaveData.playerData.player = this;
    }
	
	void Awake() {
		resources = InitResourceList();
		resourceLimits = InitResourceList();
		SelectedObjects = new LinkedList<WorldObject>();
		VisibleObjects = new LinkedList<WorldObject>();
	}

	// Use this for initialization
	void Start () {
		hud = GetComponentInChildren<HUD>();
        //Debug.Log("LayerMask.NameToLayer(\"Fog\") == "+ LayerMask.NameToLayer("Fog"));
		if(hud == null) {
			Debug.Log(username+"'s hud is null");
			Destroy(gameObject);
		} else {
			Debug.Log(username + "'s hud is okay!");
		}
		AddStartResourceLimits();
		AddStartResources();
		if(human) { // && SceneManager.GetActiveScene().name == "LoadMap") {
			setDataToPlayerSaveData();
		} else {
			if(username == "Gamma") {
				GameControl.control.Gamma = this;
			} else if(username == "Beta") {
				GameControl.control.Beta = this;
			} else if(username == "Alpha") {
				GameControl.control.Alpha = this;
			}
		}

		UserInput ui = GetComponentInChildren<UserInput>();
		ui.setPlayer(this);

		addExistingOwnedObjectsToVisibleList();
    }
	
	// Update is called once per frame
	void Update () {
		if(human) {
			hud.SetResourceValues(resources, resourceLimits);
		}
		if(findingPlacement) {
			if(tempBuilding.canPlace()){
				tempBuilding.SetTransparentMaterial(allowedMaterial, false);
			} else {
				tempBuilding.SetTransparentMaterial(notAllowedMaterial, false);
			}
		}
		if(runOnce) {
			runOnce = false;
			addVisibleEnemyObjectsToVisibleList();
		}
	}
	
	private Dictionary<ResourceType, int> InitResourceList() {
		Dictionary<ResourceType, int> list = new Dictionary<ResourceType, int>();
		list.Add(ResourceType.Gold, 0);
		list.Add(ResourceType.Power, 0);
		return list;
	}
	
	private void AddStartResourceLimits() {
		IncrementResourceLimit(ResourceType.Gold, startGoldLimit);
		IncrementResourceLimit(ResourceType.Power, startPowerLimit);
	}
	 
	private void AddStartResources() {
		AddResource(ResourceType.Gold, startGold);
		AddResource(ResourceType.Power, startPower);
	}

	public void AddResource(ResourceType type, int amount) {
		resources[type] += amount;
	}

	public int GetResource(ResourceType type) {
		return resources[type];
	}

	public void IncrementResourceLimit(ResourceType type, int amount) {
		resourceLimits[type] += amount;
	}
	
	public void AddUnit(string unitName, Vector3 spawnPoint, Vector3 rallyPoint, Quaternion rotation, Building creator) {
		Units units = GetComponentInChildren<Units>();
		GameObject newUnit = (GameObject)Instantiate(ResourceManager.GetUnit(unitName), spawnPoint, rotation);
		newUnit.transform.parent = units.transform;
		newUnit.name = unitName;
		Unit unitObject = newUnit.GetComponent<Unit>();
		if(unitObject && spawnPoint != rallyPoint){
			unitObject.SetDestination(rallyPoint);
		}
		if(unitObject) {
			unitObject.SetBuilding(creator);
			if(spawnPoint != rallyPoint){
				unitObject.SetDestination(rallyPoint);
			}
		}
	}
	
	/*************/
	/* BUILDINGS */
	/*************/

	public Building getTempBuilding() {
		return this.tempBuilding;
	}
	
	public bool CreateBuilding(string buildingName, Vector3 buildPoint, Unit creator, Rect playingArea) {
		
		tempBuilding = ResourceManager.GetBuilding(buildingName).GetComponent<Building>();
		
		if(resources[ResourceType.Gold] < tempBuilding.goldCost || resources[ResourceType.Power] < tempBuilding.dpCost){
			hud.setMessage("Not enough resources", 5.0f);
			return false;
		}
		resources[ResourceType.Gold] -= tempBuilding.goldCost;
		resources[ResourceType.Power] -= tempBuilding.dpCost;
		
		GameObject newBuilding = (GameObject)Instantiate(ResourceManager.GetBuilding(buildingName), buildPoint, new Quaternion());
		tempBuilding = newBuilding.GetComponent<Building>();
		
		if (tempBuilding) {
			tempBuilding.SetPlayer(this);
			tempCreator = creator;
			findingPlacement = true;
			tempBuilding.gameObject.name = buildingName;
            tempBuilding.hitPoints = 0;
            ResourceManager.GetMiscObject("Road").GetComponent<Road>().showPath(true);
            tempBuilding.SetTransparentMaterial(notAllowedMaterial, true);
			tempBuilding.SetColliders(false);
			tempBuilding.obstacle.enabled = false;
			tempBuilding.SetPlayingArea(playingArea);
		} else {
			Destroy(newBuilding);
			return false;
		}
		return true;
	}
	
	public bool IsFindingBuildingLocation() {
		return findingPlacement;
	}
	 
	public void FindBuildingLocation() {
		Vector3 newLocation = WorkManager.FindHitPoint(Input.mousePosition);
		newLocation.y = 0;
		tempBuilding.transform.position = newLocation;
	}
	
	public void StartConstruction() {
		findingPlacement = false;
		ResourceManager.GetMiscObject("Road").GetComponent<Road>().showPath(false);
		Buildings buildings = GetComponentInChildren<Buildings>();
		if(buildings){
			tempBuilding.transform.parent = buildings.transform;
		}
		tempBuilding.SetPlayer();
		tempBuilding.SetColliders(true);
		tempCreator.SetBuilding(tempBuilding);
		tempBuilding.StartConstruction();
		addWorldObjectToVisible(tempBuilding);
	}
	
	public void CancelBuildingPlacement() {
		findingPlacement = false;
		ResourceManager.GetMiscObject("Road").GetComponent<Road>().showPath(false);

		resources[ResourceType.Gold] += tempBuilding.goldCost;
		resources[ResourceType.Power] += tempBuilding.dpCost;
		
		Destroy(tempBuilding.gameObject);
		tempBuilding = null;
		tempCreator = null;
	}

    /************
    * SELECTION *
    ************/

    public void addWorldObjectToSelected(WorldObject obj) {
		if(obj.GetComponent<Projectile>()) {
			return;
		}
        this.SelectedObjects.AddLast(obj);
		if(isObjectVisible(obj)) {
			this.addWorldObjectToVisible(obj);
		}
    }

	public void removeWorldObjectSelection(WorldObject o) {
		if(isObjectSelected(o)) {
			o.deselect();
			this.SelectedObjects.Remove(o);
		}
	}

	public void removeAllSelected() {
		if(SelectedObjects.Count > 0) {
			foreach(WorldObject obj in SelectedObjects) {
				if(obj != null) {
					obj.deselect();
				}
			}
			this.SelectedObjects.Clear();
		}
	}

	public bool isObjectSelected(WorldObject obj) {
        return this.SelectedObjects.Contains(obj);
    }

    public void addWorldObjectToVisible(WorldObject obj) {
		if(obj.GetComponent<Projectile>()) {
			return;
		}
		this.VisibleObjects.AddLast(obj);
    }

    public void removeWorldObjectVisisble(WorldObject o) {
        if(isObjectVisible(o)) {
			removeWorldObjectSelection(o);
			this.VisibleObjects.Remove(o);
		}
    }

    public bool isObjectVisible(WorldObject obj) {
        return this.VisibleObjects.Contains(obj);
    }

	public void addExistingOwnedObjectsToVisibleList() {
		if(human) {
			WorldObject[] objs = this.GetComponentsInChildren<WorldObject>();

			foreach(WorldObject wo in objs) {
				addWorldObjectToVisible(wo);
			}
		}
	}

	public void addVisibleEnemyObjectsToVisibleList() {
		if(human) {
			WorldObject[] objs = this.GetComponentsInChildren<WorldObject>();
			LayerMask woMask = 1 << LayerMask.NameToLayer("WorldObjects");

			foreach(WorldObject wo in objs) {
				Collider[] hitColliders = Physics.OverlapSphere(wo.transform.position, wo.lineOfSight, woMask);
				for(int i = 0; i < hitColliders.Length; i++) {
					WorldObject hitWo = hitColliders[i].gameObject.GetComponentInParent<WorldObject>();
					if(hitWo != null && !hitWo.IsOwnedBy(this)) {
						addWorldObjectToVisible(hitWo);
					}
				}
			}
		}
	}

	/*******************
	* POPULATION COSTS *
	*******************/

	public int GetPop() {
		return populationCount;
	}

	public void AddToPop(int add) {
		populationCount += add;
    }

	public void RemoveFromPop(int sub) {
		populationCount -= sub;
    }

}
