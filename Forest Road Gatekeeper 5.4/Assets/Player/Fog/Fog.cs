﻿using UnityEngine;
using System.Collections;

public class Fog : MonoBehaviour {

	private FogOfWar fow;
	public bool seen = false;
	public bool active = true;

	public void setFOW(FogOfWar parent) {
		fow = parent;
	}

	public void SetTransparentMaterial() {
		Renderer[] renderers = GetComponentsInChildren<Renderer>();
		Material fogMaterial = fow.fog;
		foreach(Renderer renderer in renderers) {
			renderer.material = fogMaterial;
		}
	}

	public void SetActive(bool enabled) {
		transform.GetComponentInChildren<Renderer>().enabled = enabled;
		active = enabled;
	}
}
