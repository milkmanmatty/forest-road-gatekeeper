﻿using UnityEngine;
using System.Collections;
using RTS;

public class FogOfWar : MonoBehaviour {

	/*
		30 * 30 = 900 fog objects
		(x*10)+5, 0, (y*10)+5

		60 * 60 = 3600 fog objects
		(x*5)+2.5f, 0, (y*5)+2.5f

		120 * 120 = 14400 fog objects
		(x*2.5f)+1.25f, 0, (y*2.5f)+1.25f
	*/

	public Material fog;
	public Material black;

	void Start() {
		for(int x = 0; x < 120; x++) {
			for(int y = 0; y < 120; y++) {
				GameObject fog = (GameObject) Instantiate(ResourceManager.GetMiscObject("Fog"), new Vector3((x*2.5f)+1.25f, 0.05f, (y*2.5f)+1.25f), new Quaternion(0,0,0,0));
				fog.transform.eulerAngles = new Vector3(90f, transform.eulerAngles.y, transform.eulerAngles.z);
                fog.name = "Fog";
				Fog newFog = fog.GetComponent<Fog>();
				newFog.setFOW(this);
				newFog.transform.parent = this.transform;
            }
		}
		Debug.Log("FogOfWar created!");
	}

}
