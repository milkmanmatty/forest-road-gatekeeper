﻿using UnityEngine;
using System.Collections;

public class Revealer : MonoBehaviour {

	private WorldObject parent;
	public SphereCollider visionCollider;
	private Fog hit;

	void Awake() {
		if(parent == null) {
			parent = this.transform.parent.gameObject.GetComponent<WorldObject>();
			if(parent == null) {
				Debug.Log("parent is null");
				Destroy(this);
				return;
			}
		}
		if(visionCollider == null) {
			visionCollider = transform.GetComponent<SphereCollider>();
			if(visionCollider == null) {
				Debug.Log("visionCollider is null");
				Destroy(this);
				return;
			}
		}
	}

	void Start() {
		if(visionCollider.radius != parent.lineOfSight) {
			//Debug.Log("LoS for " + parent.objectName + " was " + visionCollider.radius + " but is now " + parent.lineOfSight);
			visionCollider.radius = parent.lineOfSight;
		}
	}

	public void UpdateRange() {
		visionCollider.radius = parent.lineOfSight;
	}

	void OnDisable() {
		visionCollider.enabled = false;

		Collider[] hitColliders = Physics.OverlapSphere(parent.transform.position, parent.lineOfSight, (1 << LayerMask.NameToLayer("Fog")) );
		for(int i = 0; i < hitColliders.Length; i++) {
			Fog fog = hitColliders[i].transform.GetComponent<Fog>();
			fog.SetActive(true);
		}
	}

	public void OnTriggerEnter(Collider other) {
		hit = other.transform.GetComponent<Fog>();
		if(!hit.seen) {
			hit.SetTransparentMaterial();
			hit.seen = true;
		}
		if(hit.active) {
			hit.SetActive(false);
		}
    }

	public void OnTriggerStay(Collider other) {
		hit = other.transform.GetComponent<Fog>();
		if(hit.active) {
			hit.SetActive(false);
		}
	}

	public void OnTriggerExit(Collider other) {
		hit = other.transform.GetComponent<Fog>();
		if(!hit.active) {
			hit.SetActive(true);
		}
	}
}
