﻿using UnityEngine;
using System.Collections;

public class NatureTree : WorldObject {

	public bool seen = false;
	public bool active = false;
	private Forest forest = null;

	protected override void Start() {
		base.Start();
		setRenderers(false);
		forest = GetComponentInParent<Forest>();
		if(forest == null) {
			Destroy(this.gameObject);
		}
    }

	public override void PerformAction(string actionToPerform) { }

	public void SetTreeMaterial() {
		Renderer renderer = GetComponentInChildren<Renderer>();
		if(active) {
			renderer.material = forest.tree;
		} else {
			renderer.material = forest.treeDark;
		}
	}

	/*************
	* COLLISIONS *
	*************/

	public override void OnTriggerEnter(Collider other) {

		if(other.gameObject.layer == LayerMask.NameToLayer("Fog")) {

			Fog fog = other.transform.GetComponent<Fog>();
			if(!fog) {
				return;
			}

			if(!fog.active) {
				seen = true;
				active = true;
				SetTreeMaterial();
				setRenderers(true);
			}

			if(fog.active && seen) {
				active = false;
				SetTreeMaterial();
				return;
			}
		}
	}

	public override void OnTriggerStay(Collider other) {
		if(other.gameObject.layer == LayerMask.NameToLayer("Fog")) {

			Fog fog = other.transform.GetComponent<Fog>();
			if(!fog) {
				return;
			}

			if(!fog.active) {
				seen = true;
				active = true;
				SetTreeMaterial();
				setRenderers(true);
			}

			if(fog.active && seen) {
				active = false;
				SetTreeMaterial();
				return;
			}
		}
	}

	public override void OnTriggerExit(Collider other) {
		Fog fog = other.transform.GetComponent<Fog>();
		if(!fog) {
			return;
		}
		if(fog.active && seen) {
			active = false;
			SetTreeMaterial();
		}

	}
}