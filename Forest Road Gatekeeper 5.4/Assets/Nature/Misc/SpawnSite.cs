﻿using UnityEngine;
using System.Collections;

public class SpawnSite : MonoBehaviour {

	void Start() {
		GameControl.control.AddSpawnSite(transform.position);
	}
}
