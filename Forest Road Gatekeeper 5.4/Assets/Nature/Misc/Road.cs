﻿using UnityEngine;
using System.Collections;

public class Road : MonoBehaviour {

	public void showPath(bool visible) {
		Renderer[] rends = GetComponentsInChildren<Renderer>();
		//Debug.Log("showPath ("+visible+") child count "+rends.Length);
		foreach(Renderer r in rends) {
			//Debug.Log(r.transform.name+" is now "+visible);
			r.enabled = visible;
		}
	}
}
