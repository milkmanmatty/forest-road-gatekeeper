﻿using UnityEngine;
using System.Collections;
using System;

public class DayNightCycle : MonoBehaviour {

	public static bool isDay = true;

	/* Daytime is from 21600 until 64800
	** Nighttime is from 64801 to 21599
	*/

	public float time = 21500;
	public int day = 1;
	public int speed;
	public float intensity;

	void Start() {
		ChangeTime();
        RenderSettings.ambientLight = new Color(intensity, intensity, intensity);

		StartCoroutine("UpdateLighting");
	}

	void Update () {
		ChangeTime();
	}

	public void ChangeTime() {
		time += Time.deltaTime * speed;
		if(time > 86400) {
			Debug.Log("New Day!");
			day += 1;
			time = 0;
		}

		if(time < 43200) {
			intensity = 1 - (43200 - time) / 43200;
		} else {
			intensity = 1 - ((43200 - time) / 43200 * -1);
		}

		intensity = (intensity + 1f) / 2.0f;
		if(intensity > 1) {
			intensity = 1;
		}
	}

	IEnumerator UpdateLighting() {
		while(true) {
			RenderSettings.ambientLight = new Color(intensity, intensity, intensity);
			yield return new WaitForSecondsRealtime(1);
		}
	}

}
