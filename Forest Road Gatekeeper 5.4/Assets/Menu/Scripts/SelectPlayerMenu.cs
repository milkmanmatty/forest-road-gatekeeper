using UnityEngine;
using RTS;
using UnityEngine.SceneManagement;

public class SelectPlayerMenu : MonoBehaviour {

	public Texture2D[] avatars;

	public GUISkin mySkin;
    public GUISkin selectionSkin;

	private int avatarIndex = -1;
     
    private string playerName = "NewPlayer";
	private const int MAX_PLAYERNAME_LENGTH = 14;
	
	void Start(){
		Debug.Log(Application.persistentDataPath + "/Player/" + playerName + ".dat");
		if(avatars.Length > 0){
			avatarIndex = 0;
		}
		ResourceManager.setAvatars(avatars);
		SelectionList.LoadEntries(PlayerSaveData.loadPlayerNames());
		SceneManager.sceneLoaded += OnSceneLoaded;
	}
    
	void OnSceneLoaded(Scene scene, LoadSceneMode m) {
		SelectionList.LoadEntries(PlayerSaveData.loadPlayerNames());
	}

	void OnGUI() {
         
        GUI.skin = mySkin;
         
        float menuHeight = GetMenuHeight();
        float groupLeft = Screen.width / 2 - ResourceManager.MenuWidth / 2;
        float groupTop = Screen.height / 2 - menuHeight / 2;
        Rect groupRect = new Rect(groupLeft, groupTop, ResourceManager.MenuWidth, menuHeight);
         
        GUI.BeginGroup(groupRect);
        //background box
        GUI.Box(new Rect(0, 0, ResourceManager.MenuWidth, menuHeight), "");
        //menu buttons
        float leftPos = ResourceManager.MenuWidth / 2 - ResourceManager.ButtonWidth - ResourceManager.Padding / 2;
		float rightPos = ResourceManager.MenuWidth / 2 + ResourceManager.Padding / 2;
		float topPos = menuHeight - ResourceManager.Padding - ResourceManager.ButtonHeight;

		if(GUI.Button(new Rect(leftPos, topPos, ResourceManager.ButtonWidth, ResourceManager.ButtonHeight), "Create New")) {
			CreatePlayer();
		}
		if(GUI.Button(new Rect(rightPos, topPos, ResourceManager.ButtonWidth, ResourceManager.ButtonHeight), "Select")) {
			SelectPlayer();
		}

		//text area for player to type new name
		float textTop = menuHeight - 2 * ResourceManager.Padding - ResourceManager.ButtonHeight - ResourceManager.TextHeight;
        float textWidth = ResourceManager.MenuWidth - 2 * ResourceManager.Padding;
        playerName = GUI.TextField(new Rect(ResourceManager.Padding, textTop, textWidth, ResourceManager.TextHeight), playerName, MAX_PLAYERNAME_LENGTH);
		SelectionList.SetCurrentEntry(playerName);

		if(avatarIndex >= 0) {
			float avatarLeft = ResourceManager.MenuWidth / 2 - avatars[avatarIndex].width / 2;
			float avatarTop = textTop - ResourceManager.Padding - avatars[avatarIndex].height;
			float avatarWidth = avatars[avatarIndex].width;
			float avatarHeight = avatars[avatarIndex].height;
			
			GUI.DrawTexture(new Rect(avatarLeft, avatarTop, avatarWidth, avatarHeight), avatars[avatarIndex]);
			
			float buttonTop = textTop - ResourceManager.Padding - ResourceManager.ButtonHeight;
			float buttonLeft = ResourceManager.Padding;
			
			
			if(GUI.Button(new Rect(buttonLeft, buttonTop, ResourceManager.ButtonHeight, ResourceManager.ButtonHeight), "<")) {
				avatarIndex -= 1;
				if(avatarIndex < 0){
					avatarIndex = avatars.Length - 1;
				}
			}
			buttonLeft = ResourceManager.MenuWidth - ResourceManager.Padding - ResourceManager.ButtonHeight;
			if(GUI.Button(new Rect(buttonLeft, buttonTop, ResourceManager.ButtonHeight, ResourceManager.ButtonHeight), ">")) {
				avatarIndex = (avatarIndex+1) % avatars.Length;
			}
		}
        GUI.EndGroup();

		//selection list, needs to be called outside of the group for the menu
		float selectionLeft = groupRect.x + ResourceManager.Padding;
		float selectionTop = groupRect.y + ResourceManager.Padding;
		float selectionWidth = groupRect.width - 2 * ResourceManager.Padding;
		float selectionHeight = groupRect.height - GetMenuItemsHeight() - ResourceManager.Padding;

		SelectionList.Draw(selectionLeft, selectionTop, selectionWidth, selectionHeight, selectionSkin);
		string newSelection = SelectionList.GetCurrentEntry();

		//set saveName to be name selected in list if selection has changed
		if(PlayerSaveData.playerData.playerName != newSelection) {

			//Set PlayerSaveData playername so that correct player is then loaded
			PlayerSaveData.playerData.playerName = newSelection;
			PlayerSaveData.playerData.load();

			//Set local playername so that correct one is shown at the bottom
			playerName = PlayerSaveData.playerData.playerName;
			avatarIndex = ResourceManager.getAvatarID(PlayerSaveData.playerData.playerImg);
		}
	}

	private float GetMenuHeight() {
		return 250 + GetMenuItemsHeight();
	}

	private float GetMenuItemsHeight() {
		float avatarHeight = 0;
		if(avatars.Length > 0){
			avatarHeight = avatars[0].height + 2 * ResourceManager.Padding;
		}
		return avatarHeight + ResourceManager.ButtonHeight + ResourceManager.TextHeight + 3 * ResourceManager.Padding;
	}

	private void CreatePlayer() {

		PlayerSaveData.playerData.setPlayerName(playerName);
		PlayerSaveData.playerData.setPlayerAvatar(avatars[avatarIndex]);
		PlayerSaveData.playerData.save();

		GetComponent<SelectPlayerMenu>().enabled = false;
		MainMenu main = GetComponent<MainMenu>();
		if(main) {
			main.enabled = true;
		}
	}

	private void SelectPlayer() {
		GetComponent<SelectPlayerMenu>().enabled = false;
		MainMenu main = GetComponent<MainMenu>();
		if(main) {
			main.enabled = true;
		}
	}
}