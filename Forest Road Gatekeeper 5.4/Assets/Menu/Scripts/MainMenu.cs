using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using RTS;
 
public class MainMenu : Menu {
 
    protected override void SetButtons () {
        buttons = new string[] {"New Game", "Load Game", "Change Player", "Quit Game"};
    }
 
    protected override void HandleButton (string text) {
        switch(text) {
            case "New Game": NewGame(); break;
			case "Load Game": LoadGame(); break;
			case "Change Player": ChangePlayer(); break;
            case "Quit Game": ExitGame(); break;
            default: break;
        }
    }
 
    private void NewGame() {
        ResourceManager.MenuOpen = false;
        SceneManager.LoadScene("Map");
        //makes sure that the loaded level runs at normal speed
        Time.timeScale = 1.0f;
    }

	private void LoadGame() {

		GameSaveData.gameData.load();

		ResourceManager.MenuOpen = false;
		SceneManager.LoadScene("LoadMap");
		Time.timeScale = 1.0f;
	}
	
	private void ChangePlayer() {
		GetComponent<MainMenu>().enabled = false;
		GetComponent<SelectPlayerMenu>().enabled = true;
		//SelectionList.LoadEntries(PlayerManager.GetPlayerNames());
	}
}