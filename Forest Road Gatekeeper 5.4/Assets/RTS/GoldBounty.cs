﻿using UnityEngine;
using System.Collections;

namespace RTS {
	public static class GoldBounty {

		private static int GoldBountyOptionsID = 0;
		private static GoldBountyOptions[] PresetOptions = null;

		public struct GoldBountyOptions {
			public int ID;
			public int minGold;
			public int maxGold;
			public int divisor;

			public GoldBountyOptions(int id, int min, int max, int div) {
				ID = id;
				minGold = min;
				maxGold = max;
				divisor = div;
			}
        }

		public static int GetGoldBountyForWave(int waveNumber, int goldBountyID) {

			if(PresetOptions == null) {
				CreatePresets();
            }
			if(goldBountyID > GoldBountyOptionsID) {
				return -1;
			}

			GoldBountyOptions gb = PresetOptions[goldBountyID];
			int baseGold = Random.Range(gb.minGold, gb.maxGold+1);
			
			return baseGold + (waveNumber / gb.divisor);
		}

		private static void CreatePresets() {

			PresetOptions = new GoldBountyOptions[5];

			PresetOptions[0] = new GoldBountyOptions(GetNextID(), 1, 3, 2);
			PresetOptions[1] = new GoldBountyOptions(GetNextID(), 2, 4, 20);
			PresetOptions[2] = new GoldBountyOptions(GetNextID(), 4, 6, 20);
			PresetOptions[3] = new GoldBountyOptions(GetNextID(), 6, 8, 20);
			PresetOptions[4] = new GoldBountyOptions(GetNextID(), 10, 12, 25);

		}

		public static int GetNextID() {
			GoldBountyOptionsID++;
            return GoldBountyOptionsID-1;
		}

		
	}
}
