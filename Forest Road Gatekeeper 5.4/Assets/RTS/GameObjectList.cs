using UnityEngine;
using System.Collections;
using RTS;

public class GameObjectList : MonoBehaviour {

	public static GameObjectList gameObjectList;

	public GameObject[] buildings;
	public GameObject[] towers;

	public GameObject[] units;
	public GameObject[] enemyUnits;

	public GameObject[] miscObjects;
	public GameObject[] projectiles;

	public GameObject[] players;

	// Use this for initialization
	void Awake() {
		if(gameObjectList == null) {
			DontDestroyOnLoad(transform.gameObject);
			ResourceManager.SetGameObjectList(this);
			gameObjectList = this;
        } else {
			Destroy(this.gameObject);
		}
	}


	/// <summary>
	/// Searches all arrays for the name of the specified parameter.
	/// Returns the first found match.
	/// </summary>
	/// <param name="name"></param>
	/// <returns>1 if building, 2 if unit, 3 if miscObject, -1 if not found</returns>
	public int findObjectType(string name) {
		if(isBuilding(name)) {
			return 1;
		}
		if(isUnit(name)) {
			return 2;
		}
		if(isMiscObject(name)) {
			return 3;
		}
		return -1;
	}
	
	public GameObject GetBuilding(string name) {
		for(int i = 0; i < buildings.Length; i++) {
			Building building = buildings[i].GetComponent<Building>();
			if(building && building.name == name){
				return buildings[i];
			}
		}
		return null;
	}

	public bool isBuilding(string name) {
		for(int i = 0; i < buildings.Length; i++) {
			Building building = buildings[i].GetComponent<Building>();
			if(building && building.name == name) {
				return true;
			}
		}
		return false;
	}

	public GameObject GetUnit(string name) {
		for(int i = 0; i < units.Length; i++) {
			Unit unit = units[i].GetComponent<Unit>();
			if(unit && unit.name == name){
				return units[i];
			}
		}
		return null;
	}

	public bool isUnit(string name) {
		for(int i = 0; i < units.Length; i++) {
			Unit unit = units[i].GetComponent<Unit>();
			if(unit && unit.name == name) {
				return true;
			}
		}
		return false;
	}

	public GameObject GetEnemyUnit(int index) {
		if(index < enemyUnits.Length || index >= 0) {
			return enemyUnits[index];
		}
		return null;
	}

	public GameObject GetMiscObject(string name) {
		foreach(GameObject worldObject in miscObjects) {
			if(worldObject.name == name) {
				return worldObject;
			}
		}
		return null;
	}

	public bool isMiscObject(string name) {
		foreach(GameObject worldObject in miscObjects) {
			if(worldObject.name == name) {
				return true;
			}
		}
		return false;
	}

	public GameObject GetProjectile(string name) {
		foreach(GameObject worldObject in projectiles) {
			if(worldObject.name == name) {
				return worldObject;
			}
		}
		return null;
	}

	public bool isProjectile(string name) {
		foreach(GameObject worldObject in projectiles) {
			if(worldObject.name == name) {
				return true;
			}
		}
		return false;
	}

	public Player GetPlayer(string name) {
		foreach(GameObject player in players) {
			if(player.GetComponent<Player>().username == name) {
				return player.GetComponent<Player>();
			}
		}
		return null;
	}

	public Player[] GetPlayers() {
		Player[] result = new Player[players.Length];
		for(int x = 0; x < players.Length; x++) {
			result[x] = players[x].GetComponent<Player>();
		}
		return result;
	}

	public bool isPlayer(string name) {
		foreach(GameObject player in players) {
			if(player.GetComponent<Player>().username == name) {
				return true;
			}
		}
		return false;
	}

	public Texture2D GetBuildImage(string name) {
		for(int i = 0; i < buildings.Length; i++) {
			Building building = buildings[i].GetComponent<Building>();
			if(building && building.name == name){
				return building.buildImage;
			}
		}
		for(int i = 0; i < units.Length; i++) {
			Unit unit = units[i].GetComponent<Unit>();
			if(unit && unit.name == name){
				return unit.buildImage;
			}
		}
		return null;
	}
}
