﻿using UnityEngine;
using System;
using System.Collections.Generic;
using RTS;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

	public static GameControl control;

	private static readonly int GAMMA_INC_PER_WAVE = 10;
	private static readonly int BETA_INC_PER_WAVE = 50;
	private static readonly int ALPHA_INC_PER_WAVE = 250;

	public Player Gamma;
	public Player Beta;
	public Player Alpha;

	public int currentWave = 0;
	private int enemiesPerSpawn;
	private float waveTimer = 0;

	private Vector3[] spawnSiteVectors = new Vector3[6];
	private int spawnSitesTaken = 0;

	private int totalEnemyTypes;
	private int enemiesSpawned;
	private int enemiesKilled;

	public int gamma_DmgHPModifier = 0;
	public int beta_DmgHPModifier = 0;
	public int alpha_DmgHPModifier = 0;
	public int speed_Modifier = 0;
	public float armour_Modifier = 0.0f;

	private int currentGoldBountyID = 0;
	public int beasetRegen = 1;

	public int[] towerLevels;
	public int[][] towerCosts;
	/* [1][0] = cost to upgrade Keep to next level
	** [1][1] = increment cost of Keep.
	** [1][2] = base upgrade cost of Keep (cost to upgrade Keep to first level).
	*/

	private string SceneName;
	private bool firstWave = true;
	private WorldObject HIDEOUT = null;


	int w = Screen.width;
	int h = Screen.height;
	GUIStyle style = new GUIStyle();

	/****************
	* Unity Methods *
	****************/

	void Awake() {
		if(control == null) {
			control = this;
			DontDestroyOnLoad(gameObject);
		} else if(control != this) {
			Destroy(gameObject);
		}

		if(SceneManager.GetActiveScene().name == "LoadMap") {
			Debug.Log("LoadMap is the scene!");
			SceneName = "LoadMap";
			Player user = insertNewPlayerInScene();
			user.setDataToPlayerSaveData();

            createUnitsFromGameSaveData();
		} else if(SceneManager.GetActiveScene().name == "Map") {
			Debug.Log("Map is the scene!");
			SetupTowerCosts();
			SceneName = "Map";
		}

		totalEnemyTypes = ResourceManager.GetTotalEnemyUnits();
    }

	void Start() {
		StartCoroutine(DisplayerTimerToNextWave());

		if(SceneName == "Map") {
			FirstWaveCountdown();
        } else if(SceneName == "LoadMap") {
			StartCoroutine(WaveEndListener());
		}
		HIDEOUT = Hideout.GetHideoutObject();
		StartCoroutine("HideoutDeathListener");
    }

	void OnGUI() {
		GUI.depth = 1;
		if(waveTimer > 0) {
			Rect rect = new Rect(w - 200, ResourceManager.RESOURCE_BAR_HEIGHT, 200, h / 50);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			string text = string.Format("Wave "+currentWave+" spawns in "+ waveTimer+" seconds");
			GUI.Label(rect, text, style);
		}
	}

	/******************
	* TOWER UPGRADING *
	******************/

	private void SetupTowerCosts() {

		towerLevels = new int[GameObjectList.gameObjectList.towers.Length];
		//All towerLevel elements are now 0.
		towerCosts = new int[GameObjectList.gameObjectList.towers.Length][];

		/* [0][0] = cost to upgrade Keep to next level
		** [0][1] = increment cost of Keep.
		** [0][2] = base upgrade cost of Keep (cost to upgrade Keep to first level).
		*/

		Building temp;

		//for each tower
		for(int i = 0; i < towerLevels.Length; i++) {
			temp = GameObjectList.gameObjectList.towers[i].GetComponent<Building>();
			towerCosts[i] = new int[3];
			towerCosts[i][0] = temp.costBaseUpgrade;
			towerCosts[i][1] = temp.costIncremenet;
			towerCosts[i][2] = temp.costBaseUpgrade;
			Debug.Log("towerCosts["+i+"] "+ towerCosts[i][0]+", "+ towerCosts[i][1] + ", " + towerCosts[i][2]);
		}
	}

	public void UpdateTowerLevelAndCosts(Building tower) {
		int towerID = GetTowerIndex(tower);
		towerLevels[towerID] += 1;
		towerCosts[towerID][0] += towerCosts[towerID][1];
	}

	public int GetTowerIndex(Building tower) {
		GameObject[] towers = GameObjectList.gameObjectList.towers;

		for(int t = 0; t < towers.Length; t++) {
			if(towers[t].gameObject.name == tower.name) {
				return t;
			}
		}
		return -1;
	}


	/********
	* OTHER *
	********/

	public Player insertNewPlayerInScene() {
		GameObject humanPlayer = (GameObject) Instantiate(ResourceManager.GetMiscObject("Player"), new Vector3(), new Quaternion());
		Player newPlayer = humanPlayer.GetComponent<Player>();
		newPlayer.gameObject.name = "Player";
        return newPlayer;
	}

	public void createUnitsFromGameSaveData() {

		string[] unitList = GameSaveData.gameData.getUnitList();
		float[][] unitLocations = GameSaveData.gameData.getUnitLocations();
		float[] unitHP = GameSaveData.gameData.getUnitHP();

		for(int i = 0; i < unitList.Length; i++) {
			int objType = ResourceManager.findObjectType(unitList[i]);

			GameObject newUnit = null;

			if(objType == 1) {
				newUnit = (GameObject) Instantiate(ResourceManager.GetBuilding(unitList[i]), new Vector3(unitLocations[i][0], unitLocations[i][1], unitLocations[i][2]), new Quaternion());
			} else if(objType == 2) {
				newUnit = (GameObject) Instantiate(ResourceManager.GetUnit(unitList[i]), new Vector3(unitLocations[i][0], unitLocations[i][1], unitLocations[i][2]), new Quaternion());
			} else {
				Debug.LogError("Cannot create unit of type: " + objType);
				continue;
			}

			WorldObject wo = (WorldObject) newUnit.GetComponent<WorldObject>();
			wo.name = unitList[i];
            wo.SetPlayer(PlayerSaveData.playerData.player);
			wo.setHPPercent(unitHP[i]);

			if(objType == 1) {
				wo.transform.parent = PlayerSaveData.playerData.player.gameObject.GetComponentInChildren<Buildings>().transform;
			} else if(objType == 2) {
				wo.transform.parent = PlayerSaveData.playerData.player.gameObject.GetComponentInChildren<Units>().transform;
			}
		}
	}



	/**********
	** WAVES **
	**********/
	public float GammaMultiplier = 1.0f;
	public float HeroMultiplier = 1.5f;
	public float BetaMultiplier = 3.0f;
	public float AlphaMultiplier = 6.0f;

	public int GetDeathGoldBounty(WorldObject dyingUnit) {

		int baseGold = GoldBounty.GetGoldBountyForWave(currentWave, currentGoldBountyID);

		if(dyingUnit.getPlayer().username == "Alpha") {
			return (int) (baseGold * AlphaMultiplier);
		} else if(dyingUnit.getPlayer().username == "Beta") {
			return (int) (baseGold * BetaMultiplier);
		} else if(dyingUnit.getPlayer().username == "Gamma") {
			if(dyingUnit.isUnit) {
				if(dyingUnit.GetComponent<Unit>().isHero) {
					return (int) (baseGold * HeroMultiplier);
				}
			} else if(dyingUnit.isBuilding) {
				return 25;
			}
		}
		return baseGold;
	}


	


	/********************
	* SPAWN UNITS/SITES *
	********************/

	public void AddSpawnSite(Vector3 newLoc) {
		if(spawnSitesTaken >= spawnSiteVectors.Length) {
			return;
		} else {
			spawnSiteVectors[spawnSitesTaken] = newLoc;
			spawnSitesTaken++;
        }
	}


	public void SpawnWave() {
		int[] selectedSpawnSites = WorkManager.GenerateRandom(4, 0, 6);

		int unitType = UnityEngine.Random.Range(0, totalEnemyTypes);

		//This many unitTypes are spawned at every spawnSite.
		int numEnemiesToSpawn = (currentWave / 5) + 1;

		GameObject gameObj = (GameObject) Instantiate(ResourceManager.GetEnemyUnit(unitType), Vector3.one, new Quaternion());
		WorldObject obj = gameObj.GetComponent<WorldObject>();
		Vector3 unitSize = obj.GetSelectionBounds().size;
		Destroy(gameObj.gameObject);

		//spawn the units
		for(int i = 0; i < selectedSpawnSites.Length; i++) {
			SpawnUnits(numEnemiesToSpawn, spawnSiteVectors[selectedSpawnSites[i]], unitType, unitSize, 0);
        }

		//What about Beta units?
		if(currentWave % 5 == 0) {
			int betaSite = UnityEngine.Random.Range(0, spawnSiteVectors.Length);
			SpawnUnits(numEnemiesToSpawn, spawnSiteVectors[betaSite], unitType, unitSize, 1);
			//Add armour additions
			armour_Modifier += 0.02f;

		//What about Alpha units?
		} else if(currentWave % 25 == 0) {
			int alphaSite = UnityEngine.Random.Range(0, spawnSiteVectors.Length);

			int alphaCount = currentWave / 25;
			//1 == 1+0; 2 == 2+1; 3 == 3+2; 4 == 4+3
			alphaCount += alphaCount-1;

			SpawnUnits(alphaCount, spawnSiteVectors[alphaSite], unitType, unitSize, 2);
		}

	}

	public void SpawnUnits(int numUnits, Vector3 centre, int unitType, Vector3 unitSize, int player) {

		int totalNumSpots = 1;
		float sqrt = 0;

		GameObject unitObject = ResourceManager.GetEnemyUnit(unitType);
        string name = unitObject.name;
		
        Player p = null;
		if(player == 0) {
			p = this.Gamma;
		} else if(player == 1) {
			p = this.Beta;
		} else if(player == 2) {
			p = this.Alpha;
		}
		Units playerUnits = p.GetComponentInChildren<Units>();

		GameObject newUnit;

		for(int i = 1; i <= numUnits; i++) {
			if(i == 1) {
				//Debug.Log("spawning at centre " + centre);
				newUnit = (GameObject) Instantiate(unitObject, centre, new Quaternion());
			}
			else {
				if(i > totalNumSpots) {
					totalNumSpots += 8;
				}

				sqrt = Mathf.Sqrt(totalNumSpots);
				int j = i - (totalNumSpots - 8);

				if(j > 1- (totalNumSpots - sqrt)) {
					j += totalNumSpots - 8;
				} else if(j > 1+sqrt){
					j += (int)  ((sqrt - 2) * ((j - sqrt) / 2));
				}

				float newX = centre.x + (-4 + (j % (1 + sqrt)) + (j / (sqrt + 1))) * unitSize.x;
				float newZ = centre.z + (-3 + (j / (1 + sqrt))) * unitSize.z;
				Vector3 spawnLoc = new Vector3(newX, centre.y, newZ);

				//Debug.Log("spawning at "+ spawnLoc);

				newUnit = (GameObject) Instantiate(unitObject, spawnLoc, new Quaternion());				
            }

			newUnit.transform.parent = playerUnits.transform;
			newUnit.name = name;
			Unit wo = newUnit.GetComponent<Unit>();
			wo.SetPlayer(p);
		}
	}




	/**************
	* WAVE TIMERS *
	**************/

	private float GetDifficultyFirstWaveTime() {
		return 5;
	}

	System.Collections.IEnumerator WaitSpawnWave() {
		currentWave++;
		PlayerSaveData.playerData.player.hud.localWave = currentWave;
		CheckForWave(currentWave);
		if(currentWave == 101) {
			Debug.LogError("ENDGAME");
		}
        yield return new WaitForSeconds(waveTimer);
		SpawnWave();
		StartCoroutine(WaveEndListener());
	}

	System.Collections.IEnumerator DisplayerTimerToNextWave() {

		while(true) {
			while(waveTimer > 0 && currentWave <= 100) {
				waveTimer--;
				yield return new WaitForSeconds(1f);
			}
			yield return null;
		}

	}

	void FirstWaveCountdown() {
		while(firstWave) {
			if(currentWave == 0 && SceneName == "Map") {
				firstWave = false;
				waveTimer = GetDifficultyFirstWaveTime();
				StartCoroutine(WaitSpawnWave());
			}
		}
	}

	System.Collections.IEnumerator WaveEndListener() {
		while(currentWave < 100) {
			yield return new WaitForSeconds(0.5f);
			if(currentWave > 0 && waveTimer < 1) {
				if(Alpha.GetPop() == 0 && Beta.GetPop() == 0 && Gamma.GetPop() == 0) {

					gamma_DmgHPModifier += GAMMA_INC_PER_WAVE;
					beta_DmgHPModifier += BETA_INC_PER_WAVE;
					alpha_DmgHPModifier += ALPHA_INC_PER_WAVE;

					waveTimer = 7.0f;
					StartCoroutine(WaitSpawnWave());
					break;
				}
			}
		}
		//Debug.Log("WaveEndListener has completed");
	}

	/*************
	** END GAME **
	*************/

	System.Collections.IEnumerator HideoutDeathListener() {
		while(HIDEOUT != null) {
			yield return new WaitForSeconds(3.0f);
		}
		Debug.Log("Hideout object has fallen");
		Debug.LogError("GAME OVER");
	}

	/*****************
	* SPECIFIC WAVES *
	*****************/

	public void IncreaseEnemyModifers(int percent, bool gamma=true, bool beta=true, bool alpha=true) {
		if(gamma) { gamma_DmgHPModifier += percent; }
		if(beta) { beta_DmgHPModifier += percent; }
		if(alpha) { alpha_DmgHPModifier += percent; }
	}

	private void CheckForWave(int waveNum) {
		switch(waveNum) {
			case 10:
				Wave10();
				break;
			case 25:
				Wave25();
				break;
			case 50:
				Wave50();
				break;
			case 60:
				Wave60();
				break;
			case 75:
				Wave75();
				break;
			case 90:
				Wave90();
				break;
			case 95:
				Wave95();
				break;
			case 99:
				Wave99();
				break;
			case 100:
				Wave100();
				break;
		}
	}

	private void Wave10() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 100);
		IncreaseEnemyModifers(50);
		beasetRegen = 2;
		currentGoldBountyID = 1;
    }

	private void Wave25() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 200);
		IncreaseEnemyModifers(250);
		speed_Modifier += 10;
		beasetRegen = 3;
		currentGoldBountyID = 2;
		//increase builder HP by 500, armor by 30%
	}

	private void Wave50() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 350);
		IncreaseEnemyModifers(500, true, false, false);
		IncreaseEnemyModifers(750, false, true, false);
		IncreaseEnemyModifers(1000, false, true, true);
		speed_Modifier += 10;
		beasetRegen = 4;
		currentGoldBountyID = 3;
	}

	private void Wave60() {
		IncreaseEnemyModifers(50);
	}

	private void Wave75() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 750);
		speed_Modifier += 10;
		IncreaseEnemyModifers(500, true, false, false);
		IncreaseEnemyModifers(1000, false, true, false);
		IncreaseEnemyModifers(1500, false, true, true);
	}

	private void Wave90() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 1000);
		beasetRegen = 5;
		currentGoldBountyID = 4;
	}

	private void Wave95() {
		speed_Modifier += 10;
	}

	private void Wave99() {
		PlayerSaveData.playerData.player.AddResource(ResourceType.Gold, 750);
		IncreaseEnemyModifers(750);
	}

	private void Wave100() {
		//spawn dragons?
	}

}