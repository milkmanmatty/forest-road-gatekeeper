using UnityEngine;
using System.Collections.Generic;
using System;

namespace RTS {
    public static class WorkManager {

		public static readonly int ignoreVisionMask = ~(1 << LayerMask.NameToLayer("Vision") | 1 << LayerMask.NameToLayer("Hideout"));
		public static readonly int ignoreFogMask = ~(1 << LayerMask.NameToLayer("Fog"));

		public static Rect CalculateHealthBarPos(Bounds selectionBounds) {
			//shorthand for the coordinates of the centre of the selection bounds
			float cx = selectionBounds.center.x;
			float cy = selectionBounds.center.y;
			float cz = selectionBounds.center.z;
			//shorthand for the coordinates of the extents of the selection bounds
			float ex = selectionBounds.extents.x;
			float ey = selectionBounds.extents.y;
			float ez = selectionBounds.extents.z;

			//Determine the screen coordinates for the corners of the selection bounds
			List<Vector3> corners = new List<Vector3>();
			/*corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, 0, cz + ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx + ex, 0, cz - ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, 0, cz + ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx - ex, 0, cz - ez)));*/

			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx+ex, cy+ey, cz+ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx+ex, cy+ey, cz-ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx+ex, cy-ey, cz+ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx-ex, cy+ey, cz+ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx+ex, cy-ey, cz-ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx-ex, cy-ey, cz+ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx-ex, cy+ey, cz-ez)));
			corners.Add(Camera.main.WorldToScreenPoint(new Vector3(cx-ex, cy-ey, cz-ez)));

			//Determine the bounds on screen for the selection bounds
			Bounds screenBounds = new Bounds(corners[0], Vector3.zero);
			for(int i = 1; i < corners.Count; i++) {
				screenBounds.Encapsulate(corners[i]);
			}

			//Screen coordinates start in the bottom left corner, rather than the top left corner
			//this correction is needed to make sure the selection box is drawn in the correct place
			float selectBoxTop = screenBounds.center.y + screenBounds.extents.y;
			float selectBoxLeft = screenBounds.center.x - screenBounds.extents.x;
			float selectBoxWidth = 2 * screenBounds.extents.x;
			float selectBoxHeight = 2 * screenBounds.extents.y;

			return new Rect(selectBoxLeft, selectBoxTop, selectBoxWidth, selectBoxHeight);
		}

		public static GameObject FindHitObject(Vector3 origin) {
			Ray ray = Camera.main.ScreenPointToRay(origin);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit, 100, ignoreVisionMask)) {
				if(hit.collider.gameObject.layer == LayerMask.NameToLayer("BuildingPlacement")) {
					return hit.collider.gameObject.transform.parent.gameObject;
				} else {
					return hit.collider.gameObject;
				}
			}
			return null;
		}
		
		public static Vector3 FindHitPoint(Vector3 origin) {
			Ray ray = Camera.main.ScreenPointToRay(origin);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit, 100, ignoreVisionMask)) {
				return hit.point;
			}
			return ResourceManager.InvalidPosition;
		}

		public static List<WorldObject> FindNearbyObjects(Vector3 position, float range, bool humanPlayer, WorldObject caller) {
			Collider[] hitColliders = Physics.OverlapSphere(position, range, (1 << LayerMask.NameToLayer("WorldObjects")) );
			HashSet<int> nearbyObjectIds = new HashSet<int>();
			List<WorldObject> nearbyObjects = new List<WorldObject>();

			for(int i = 0; i < hitColliders.Length; i++) {

				if(hitColliders[i].transform.root == caller.transform.root) {
					continue;
				}
				if(ResourceManager.isProjectile(hitColliders[i].name)) {
					continue;
				}

				WorldObject closeObject = hitColliders[i].GetComponent<WorldObject>();

				if(humanPlayer) {
					if(closeObject && !closeObject.IsOwnedBy(PlayerSaveData.playerData.player) &&  closeObject.getPlayer().username != "Mother Nature" &&
                        closeObject.ObjID != 0 && !nearbyObjectIds.Contains(closeObject.ObjID)) {
						nearbyObjectIds.Add(closeObject.ObjID);
						nearbyObjects.Add(closeObject);
						//Debug.Log("Added "+parentObject.objectName);
					}
				} else {
					if(closeObject && closeObject.IsOwnedBy(PlayerSaveData.playerData.player) && 
						closeObject.ObjID != 0 && !nearbyObjectIds.Contains(closeObject.ObjID)) {
						nearbyObjectIds.Add(closeObject.ObjID);
						nearbyObjects.Add(closeObject);
					}
				}
			}

			return nearbyObjects;
		}


		/*****************************************************************************************************
		**																									**
		**	RANDOM NUMBER GENERATOR																			**
		**																									**
		**	Shamelessly take from:																			**
		**	http://codereview.stackexchange.com/questions/61338/generate-random-numbers-without-repetitions **
		**	All credit goes to rolfl and mjolka																**
		**																									**
		*****************************************************************************************************/


		static System.Random random = new System.Random();

		// 

		/// <summary>
		/// Generates count unique random numbers between min and max.
		/// Note, max is exclusive here!
		/// </summary>
		/// <param name="count">amount of numbers needed</param>
		/// <param name="min">the lowest possible number</param>
		/// <param name="max">the highest possible number +1</param>
		/// <returns></returns>
		public static int[] GenerateRandom(int count, int min, int max) {

			//  initialize set S to empty
			//  for J := N-M + 1 to N do
			//    T := RandInt(1, J)
			//    if T is not in S then
			//      insert T in S
			//    else
			//      insert J in S
			//
			// adapted for C# which does not have an inclusive Next(..)
			// and to make it from configurable range not just 1.

			if(max <= min || count < 0 ||
					// max - min > 0 required to avoid overflow
					(count > max - min && max - min > 0)) {

				// need to use 64-bit to support big ranges (negative min, positive max)
				throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
						" (" + ((Int64) max - (Int64) min) + " values), or count " + count + " is illegal");
			}

			// generate count random values.
			HashSet<int> candidates = new HashSet<int>();

			// start count values before max, and end at max
			for(int top = max - count; top < max; top++) {
				// May strike a duplicate.
				// Need to add +1 to make inclusive generator
				// +1 is safe even for MaxVal max value because top < max
				if(!candidates.Add(random.Next(min, top + 1))) {
					// collision, add inclusive max.
					// which could not possibly have been added before.
					candidates.Add(top);
				}
			}

			// load them in to a list, to sort
			int[] result = new int[candidates.Count];
			candidates.CopyTo(result);

			return result;
		}

	}
}