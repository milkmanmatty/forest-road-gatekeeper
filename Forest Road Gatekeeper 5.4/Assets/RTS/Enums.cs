
namespace RTS {
    public enum CursorState { Select, Move, Attack, PanLeft, PanRight, PanUp, PanDown, Harvest, RallyPoint }
	
	public enum ResourceType { Gold, Power, Unknown }
	
	public enum DamageType { Hack, Piece, Magic, Pure }

	public enum UnitState : int { Idle, Moving, MovingToAttack, Attacking, MovingToBuild, Building }

	public static class saveArrays {
		public static string[] UnitTypes = { "Basic Builder", "Improved Builder",
			"Advanced Builder", "Hideout", "Shrine of Healing", "Archer", "Keep" };
	}
}
