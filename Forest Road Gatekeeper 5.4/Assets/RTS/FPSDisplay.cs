﻿using UnityEngine;

public class FPSDisplay : MonoBehaviour {
	float deltaTime = 0.0f;
	int width = Screen.width / 50;

	void Update() {
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
	}

	void OnGUI() {
		int w = Screen.width, h = Screen.height;

		GUIStyle style = new GUIStyle();
		GUI.depth = 0;

		Rect rect = new Rect(w - (width*10), 5, width, h / 50);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h / 50;
		style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
		GUI.Label(rect, text, style);
	}
}