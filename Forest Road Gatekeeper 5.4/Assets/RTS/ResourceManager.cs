using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
namespace RTS {
    public static class ResourceManager {

		/******************/
		/* Player Avatars */
		/******************/
		private static Texture2D[] avatars;

		public static void setAvatars(Texture2D[] avs) {
			avatars = avs;
		}

		public static int getAvatarID(Texture2D av) {
			for(int i = 0; i < avatars.Length; i++) {
				if(avatars[i] == av) {
					return i;
				}
			}
			return -1;
		}

		public static Texture2D getAvatarFromID(int avID) {
			return avatars[avID];
		}

		/**************/
		/* Pause Menu */
		/**************/
		public static bool MenuOpen { get; set; }
		
		private static float buttonHeight = 40;
		private static float headerHeight = 32;
		private static float headerWidth = 256;
		private static float textHeight = 25;
		private static float padding = 10;
		
		public static float PauseMenuHeight { get { return headerHeight + 2 * buttonHeight + 4 * padding; } }
		public static float MenuWidth { get { return headerWidth + 2 * padding; } }
		public static float ButtonHeight { get { return buttonHeight; } }
		public static float ButtonWidth { get { return (MenuWidth - 3 * padding) / 2; } }
		public static float ButtonPadding {  get { return padding; } }
		
		public static float HeaderHeight { get { return headerHeight; } }
		public static float HeaderWidth { get { return headerWidth; } }
		public static float TextHeight { get { return textHeight; } }
		public static float Padding { get { return padding; } }

		/*******/
		/* HUD */
		/*******/
		public static int ORDERS_BAR_HEIGHT = 128;
		public static int RESOURCE_BAR_HEIGHT = 30;
		public static int SELECTION_NAME_HEIGHT = 20;
		
		public static float ScrollSpeed { get { return 35; } }
		public static float RotateAmount { get { return 10; } }
		
		public static float ZoomSpeed { get { return 5; } }
		public static float MinCameraHeight { get { return 10; } }
		public static float MaxCameraHeight { get { return 50; } }
		
		public static int ScrollWidth { get { return 25; } }
		
		private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
		public static Vector3 InvalidPosition { get { return invalidPosition; } }
		
		private static Bounds invalidBounds = new Bounds(new Vector3(-99999, -99999, -99999), new Vector3(0, 0, 0));
		public static Bounds InvalidBounds { get { return invalidBounds; } }
		
		private static GUISkin selectBoxSkin;
		public static GUISkin SelectBoxSkin { get { return selectBoxSkin; } }
		 
		public static void StoreSelectBoxItems(GUISkin skin, Texture2D healthy, Texture2D damaged, Texture2D critical) {
			selectBoxSkin = skin;
			healthyTexture = healthy;
			damagedTexture = damaged;
			criticalTexture = critical;
		}
		
		public static int BuildSpeed { get { return 2; } }
		
		private static GameObjectList gameObjectList;
		public static void SetGameObjectList(GameObjectList objectList) {
			gameObjectList = objectList;
		}

		/// <summary>
		/// Searches all arrays for the name of the specified parameter.
		/// Returns the first found match.
		/// </summary>
		/// <param name="name"></param>
		/// <returns>1 if building, 2 if unit, 3 if miscObject, -1 if not found</returns>
		public static int findObjectType(string name) {
			return gameObjectList.findObjectType(name);
		}

		public static GameObject GetBuilding(string name) {
			return gameObjectList.GetBuilding(name);
		}
		 
		public static GameObject GetUnit(string name) {
			return gameObjectList.GetUnit(name);
		}

		public static int GetTotalEnemyUnits() {
			return gameObjectList.enemyUnits.Length;
		}

		public static GameObject GetEnemyUnit(int index) {
			return gameObjectList.GetEnemyUnit(index);
		}

		public static GameObject GetMiscObject(string name) {
			return gameObjectList.GetMiscObject(name);
		}

		public static GameObject GetProjectile(string name) {
			return gameObjectList.GetProjectile(name);
		}

		public static bool isProjectile(string name) {
			return gameObjectList.isProjectile(name);
		}

		public static Player GetPlayer(string name) {
			return gameObjectList.GetPlayer(name);
		}

		public static Player[] GetPlayers() {
			return gameObjectList.GetPlayers();
		}

		public static bool isPlayer(string name) {
			return gameObjectList.GetPlayer(name);
		}

		public static Texture2D GetBuildImage(string name) {
			return gameObjectList.GetBuildImage(name);
		}
		
		private static Texture2D healthyTexture, damagedTexture, criticalTexture;
		public static Texture2D HealthyTexture { get { return healthyTexture; } }
		public static Texture2D DamagedTexture { get { return damagedTexture; } }
		public static Texture2D CriticalTexture { get { return criticalTexture; } }
		
		public static Texture2D GetResourceHealthBar(ResourceType resourceType) {
			if(resourceHealthBarTextures != null && resourceHealthBarTextures.ContainsKey(resourceType)){
				return resourceHealthBarTextures[resourceType];
			}
			return null;
		}
		
		public static void SetResourceHealthBarTextures(Dictionary<ResourceType, Texture2D> images) {
			resourceHealthBarTextures = images;
		}
		
		private static Dictionary<ResourceType, Texture2D> resourceHealthBarTextures;

        /*****************************
        DRAW SELECTION WHITE RECTANGLE
        *****************************/

        static Texture2D whiteTexture;
        public static Texture2D WhiteTexture {
            get {
                if (whiteTexture == null) {
                    whiteTexture = new Texture2D(1, 1);
                    whiteTexture.SetPixel(0, 0, Color.white);
                    whiteTexture.Apply();
                }

                return whiteTexture;
            }
        }

        public static void DrawScreenRect(Rect rect, Color color) {
            GUI.color = color;
            GUI.DrawTexture(rect, WhiteTexture);
            GUI.color = Color.white;
        }

        public static void DrawScreenRectBorder(Rect rect, float thickness, Color color) {
            // Top
            DrawScreenRect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
            // Left
            DrawScreenRect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
            // Right
            DrawScreenRect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
            // Bottom
            DrawScreenRect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
        }

        public static Rect GetScreenRect(Vector3 screenPosition1, Vector3 screenPosition2) {
            // Move origin from bottom left to top left
            screenPosition1.y = Screen.height - screenPosition1.y;
            screenPosition2.y = Screen.height - screenPosition2.y;
            // Calculate corners
            var topLeft = Vector3.Min(screenPosition1, screenPosition2);
            var bottomRight = Vector3.Max(screenPosition1, screenPosition2);
            // Create Rect
            return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
        }

        public static Bounds GetViewportBounds(Camera camera, Vector3 screenPosition1, Vector3 screenPosition2) {
            var v1 = Camera.main.ScreenToViewportPoint(screenPosition1);
            var v2 = Camera.main.ScreenToViewportPoint(screenPosition2);
            var min = Vector3.Min(v1, v2);
            var max = Vector3.Max(v1, v2);
            min.z = camera.nearClipPlane;
            max.z = camera.farClipPlane;

            var bounds = new Bounds();
            bounds.SetMinMax(min, max);
            return bounds;
        }

    }
}