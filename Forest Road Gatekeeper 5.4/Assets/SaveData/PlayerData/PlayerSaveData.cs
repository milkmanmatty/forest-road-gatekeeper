﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using RTS;

public class PlayerSaveData : MonoBehaviour {

	public static PlayerSaveData playerData;
	public Player player;

	public string playerName = "";
	public Texture2D playerImg = null;

	public int deityID = 0;
	public string dietyName = "";

	public int highestWaveReached = 0;
	public int mostGoldEarned = 0;
	public int mostUnitsKilled = 0;

	void Awake() {
		if(playerData == null) {
			playerData = this;
			DontDestroyOnLoad(gameObject);
			System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/Player/");
		} else if(playerData != this) {
			Destroy(gameObject);
		}
	}

	public void save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/Player/" + playerName + ".dat");

		PlayerData data = new PlayerData();
		data.playerName = playerName;
		data.playerAvatarID = ResourceManager.getAvatarID(playerImg);
		data.highestWaveReached = highestWaveReached;
		data.mostGoldEarned = mostGoldEarned;
		data.mostUnitsKilled = mostUnitsKilled;

		bf.Serialize(file, data);
		file.Close();
	}

	public void load() {
		if(File.Exists(Application.persistentDataPath + "/Player/" + playerName + ".dat")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/Player/" + playerName + ".dat", FileMode.Open);
			PlayerData data = (PlayerData) bf.Deserialize(file);
			file.Close();

			playerName = data.playerName;
			playerImg = ResourceManager.getAvatarFromID(data.playerAvatarID);
			highestWaveReached = data.highestWaveReached;
			mostGoldEarned = data.mostGoldEarned;
			mostUnitsKilled = data.mostUnitsKilled;
		} else {
			Debug.LogError("File not found");
		}
	}

	public string getPlayerName() {
		return playerName;
	}

	public void setPlayerName(string name) {
		playerName = name;
	}

	public Texture2D getPlayerAvatar() {
		return playerImg;
	}

	public void setPlayerAvatar(Texture2D av) {
		playerImg = av;
	}

	public static string[] loadPlayerNames() {
		string[] names = System.IO.Directory.GetFiles(Application.persistentDataPath + "/Player/");
		for(int i = 0; i < names.Length; i++) {
			names[i] = System.IO.Path.GetFileNameWithoutExtension(names[i]);
		}
		return names;
	}
}

[Serializable]
class PlayerData {
	public string playerName;
	public int playerAvatarID;

	public int highestWaveReached;
	public int mostGoldEarned;
	public int mostUnitsKilled;
}