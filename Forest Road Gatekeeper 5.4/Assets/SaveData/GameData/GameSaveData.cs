﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using RTS;

public class GameSaveData : MonoBehaviour {

	public static GameSaveData gameData;

	private int wave;
	private int gold;
	private int dp;
	private int enemiesKilled;

	private int[] towerLevels;

	private string[] unitList;
	private float[][] unitLocations;
	private float[] unitHP;

	void Awake () {
		if(gameData == null) {
			gameData = this;
			DontDestroyOnLoad(gameObject);
			System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/Game/");
        } else if(gameData != this) {
			Destroy(gameObject);
		}
	}

	public void fetchLatestData() {
		object[] obj = FindObjectsOfType(typeof(WorldObject));
		Debug.Log("Attempting to save "+obj.Length+" WorldObjects");

		int playerOwnedWorldObjects = 0;
		WorldObject[] worldObjects = new WorldObject[0];

		foreach(object o in obj) {
			WorldObject wo = (WorldObject) o;
			Debug.Log(wo.name +" owned by "+ wo.getPlayer().username+"("+ PlayerSaveData.playerData.getPlayerName()+")");
			if(wo.getPlayer().username == PlayerSaveData.playerData.getPlayerName()) {
				playerOwnedWorldObjects++;
				worldObjects = increaseArraySize(worldObjects, wo);
			}
		}

		Debug.Log("Grabbed " + playerOwnedWorldObjects+" / "+worldObjects.Length + " player WorldObjects");

		unitList = new string[playerOwnedWorldObjects];
		unitHP = new float[playerOwnedWorldObjects];
		unitLocations = new float[playerOwnedWorldObjects][];
		initializeUnitLocationArray();

		WorldObject temp;
		Vector3 tempV = new Vector3();
        for(int i = 0; i < playerOwnedWorldObjects; i++) {
			temp = worldObjects[i];
			tempV = temp.transform.position;

			unitList[i] = temp.name;
			unitHP[i] = (float) temp.hitPoints / (float) temp.maxHitPoints;
			unitLocations[i][0] = tempV.x;
			unitLocations[i][1] = tempV.y;
			unitLocations[i][2] = tempV.z;
		}

	}

	private WorldObject[] increaseArraySize(WorldObject[] array, WorldObject objToAdd) {

		WorldObject[] result = new WorldObject[array.Length + 1];

		for(int i = 0; i < array.Length; i++) {
			result[i] = array[i];
		}
		result[result.Length - 1] = objToAdd;

		return result;
	}

	private void initializeUnitLocationArray() {
		for(int i = 0; i < unitLocations.Length; i++) {
			unitLocations[i] = new float[3];
		}
	}

	public void save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/Game/" + PlayerSaveData.playerData.getPlayerName() + ".dat");

		GameData data = new GameData();
		data.wave = wave;
		data.gold = gold;
		data.dp = dp;
		data.enemiesKilled = enemiesKilled;
		data.towerLevels = towerLevels;

		data.unitList = unitList;
		data.unitLocations = unitLocations;
		data.unitHP = unitHP;

		bf.Serialize(file, data);
		file.Close();
	}

	public bool load() {
		if(File.Exists(Application.persistentDataPath + "/Game/" + PlayerSaveData.playerData.getPlayerName() + ".dat")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/Game/" + PlayerSaveData.playerData.getPlayerName() + ".dat", FileMode.Open);
			GameData data = (GameData) bf.Deserialize(file);
			file.Close();

			wave = data.wave;
			gold = data.gold;
			dp = data.dp;
			enemiesKilled = data.enemiesKilled;
			towerLevels = data.towerLevels;
			unitHP = data.unitHP;

			unitList = data.unitList;
			unitLocations = data.unitLocations;

			Debug.Log("UnitList: "+data.unitList.Length);
			foreach(string s in data.unitList) {
				Debug.Log(s);
			}

			return true;
		}
		return false;
	}

	public string[] getUnitList() {
		return unitList;
	}

	public float[][] getUnitLocations() {
		return unitLocations;
	}

	public float[] getUnitHP() {
		return unitHP;
	}

	public int[] getTowerLevels() {
		return towerLevels;
	}
}

[Serializable]
class GameData {
	public int wave;
	public int gold;
	public int dp;
	public int enemiesKilled;

	public int[] towerLevels;

	public string[] unitList;
	public float[][] unitLocations;
	public float[] unitHP;
}